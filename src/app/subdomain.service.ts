import { Injectable } from '@angular/core';
import { CommonService } from './Services/Commonservices';
import { GLOBAL } from './shared/global';
import { ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubdomainService {
  private isValidDomainSubject = new ReplaySubject<boolean>(1);
  public isValidDomain = this.isValidDomainSubject.asObservable();
  
  constructor(private commonHttp:CommonService) { }

  GetSubDomainUrl()
  {
    debugger;
      let hostName = window.location.host;
      let fullUrlName = window.location.href;
      let subdomain = null;
      if(hostName === 'localhost:50001'||hostName === 'localhost:4200'||hostName === 'localhost:4400'||hostName==="52.25.96.244:7002"||hostName==="127.0.0.1:5500"){
        hostName = 'Paymaster.paymaster.com';
      }
     const splitHostName = hostName.split('.');
      if (splitHostName.length >= 3) {
        subdomain = splitHostName[0];
      }
      // console.log(subdomain)
    return subdomain;
  }

  verifyDomainName(domainName: string) {
    let Url=GLOBAL.fetchdomainlist+'?DomainName='+domainName;
    return this.commonHttp.Get(Url).pipe(map(res => {
      if(res){
      debugger;
      localStorage.setItem("domainToken",res.accessToken);
      this.isValidDomainSubject.next(true)
    }
      else
      this.isValidDomainSubject.next(false)
    }))
  }

}
