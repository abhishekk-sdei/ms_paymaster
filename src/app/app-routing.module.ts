import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubdomainGuard } from './subDomain.guard';
import { ToastrModule } from 'ngx-toastr';

const routes: Routes = [
 
  {
      path:'',
      canActivate: [SubdomainGuard],
      loadChildren :()=>import('./organization-portal/organization-portal.module').then(m => m.OrganizationPortalModule)
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),ToastrModule.forRoot()],
  exports: [RouterModule]
})
export class AppRoutingModule { }
