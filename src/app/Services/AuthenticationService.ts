import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { inject } from '@angular/core/testing';
import { GLOBAL } from '../shared/global';

@Injectable()
export class AuthenticationService {
    
    
    constructor(private http: HttpClient,) { }
    
    organizationlogin(Logindetails:any){
        let url=GLOBAL.LoginUrl;
        const headers = new HttpHeaders({
            'accessToken': localStorage.getItem('domainToken'),
        });

        return this.http.post<any>(`${url}`, Logindetails, { headers: headers })
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }

    organizationlogout(){
        
    }

    
}