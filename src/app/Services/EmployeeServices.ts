import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { inject } from '@angular/core/testing';
import { GLOBAL } from '../shared/global';
import { CommonService } from './Commonservices';
@Injectable()
export class EmployeeService {
    
     url:string="";
    constructor(private http: HttpClient,private commonservice:CommonService) { }

    GetEmployeeList(pageNumber: number, pageSize: number,TimePunchCheck:number = 0){
    this.url=GLOBAL.GetEmployeelist;
    // const headers = new HttpHeaders({
    //     'domaintoken': localStorage.getItem('domainToken'),
    //     // 'Authorization': 'Bearer '+localStorage.getItem('accessToken'),
    //     'Authorization': localStorage.getItem("accessToken"),
    // })
    const queryParams = `?TimePunchCheck=${TimePunchCheck}&page=${pageNumber}&pageSize=${pageSize}`
    return this.http.get<any>(`${this.url}${queryParams}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
    }));
    }

    SocSecNoExistOrnot(SocSecNo:string,EmpID:string){
        let url=GLOBAL.CheckSocSecNo;
        // const headers = new HttpHeaders({
        //     // 'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        // })
        const queryParams = `?SocSecNo=${SocSecNo}&empid=${EmpID}`
        return this.http.get<any>(`${url}${queryParams}`)
            .pipe(map(response => {
    
                if (response) {
                    console.log(response);
                    return response;
                } else if (response && response.data) {
                   
                }
                // return response;
        }));
    }
    

    AddOrganization(organizationDetails:any){
        let url=GLOBAL.AddOrganization;
        const headers = new HttpHeaders({
            // 'domaintoken': localStorage.getItem('domainToken'),
            // 'Authorization': localStorage.getItem('accessToken'),
        })
        return this.http.post<any>(`${url}`, organizationDetails, { headers: headers })
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            if (response ) {
                // console.log(response);
                return response;
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
           
        }));
    }
    AddEmployee(employeeDetails:any){
        debugger;
        let url=GLOBAL.AddEmployee;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }
    AddEmployeeBasicDetails(employeeDetails:any){
        debugger;
        let url=GLOBAL.AddEmployeeBasicDetails;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }
    
    AddEmployeeSalaryDetails(employeeDetails:any){
        debugger;
        let url=GLOBAL.AddEmployeeSalaryDetails;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }
    AddEmployeePersonalDetails(employeeDetails:any){
        debugger;
        let url=GLOBAL.AddEmployeePersonalDetails;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }
    
    UpdateEmployeeBasicDetails(employeeDetails:any){
        debugger;
        let url=GLOBAL.UpdateEmployeeBasicDetails;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }
    
    UpdateEmployeeSalaryDetails(employeeDetails:any){
        debugger;
        let url=GLOBAL.UpdateEmployeeSalaryDetails;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }
    UpdateEmployeePersonalDetails(employeeDetails:any){
        debugger;
        let url=GLOBAL.UpdateEmployeePersonalDetails;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }

    UpdateEmployee(employeeDetails:any){
        let url=GLOBAL.UpdateEmployee;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }


    Deleted(EmployeeId:number){
        debugger;
        let url=GLOBAL.DeletedEmployee;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization':localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}?EmployeeId=${EmployeeId}`, {} )
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }

    GetEmployeeListByName(Seaching:string,pageNumber: number, pageSize: number){
        this.url=GLOBAL.GetEmployeeListByName;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?Seaching=${Seaching}&page=${pageNumber}&pageSize=${pageSize}`
        return this.http.get<any>(`${this.url}${queryParams}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
            }));
    }

    GetGenderList(){
        this.url=GLOBAL.Genderlist;
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }

    GetPayFrequencyList(){
        this.url=GLOBAL.GetPayFrequencyList;
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }
    
    GetRaceList(){
        this.url=GLOBAL.Racelist;
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }

    

    GetMaritalList(){
        this.url=GLOBAL.maritallist;
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }

    GetCountryList(){
        this.url=GLOBAL.Countrylist;
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }
    
    GetStateList(CountryId:number){
        debugger;
        this.url=GLOBAL.statelist;
        return this.http.get<any>(`${this.url}?CountryId=${CountryId}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }

    GetCityList(CountryId:number,StateID:number){
        this.url=GLOBAL.citylist;
        return this.http.get<any>(`${this.url}?CountryId=${CountryId}&StateId=${StateID}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }

    GetEmployeeById(EmployeeID:number){
        this.url=GLOBAL.GetEmployeeId;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?EmpId=${EmployeeID}`
        return this.http.get<any>(`${this.url}${queryParams}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
            }));
    }
    GetEmployeeTimepunches(EmployeeId:number){
        this.url=GLOBAL.GetTimePunchList;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?EmpoyeeID=${EmployeeId}`
        return this.http.get<any>(`${this.url}${queryParams}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
            }));
    }

    GetEmployeeTaxDeduction(_CheckId:number){
        this.url=GLOBAL.GetEmployeeCheckDeductionList;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?CheckId=${_CheckId}`
        return this.http.get<any>(`${this.url}${queryParams}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
            }));
    }

    ViewEmployeeTimepunches(EmployeeId:number){
        this.url=GLOBAL.ViewTimePunchList;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?EmpoyeeID=${EmployeeId}`
        return this.http.get<any>(`${this.url}${queryParams}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
            }));
    }

    ViewChecksDetail(EmployeeID:number,PayPeriodId:number,ApplicationId:number=0){
        this.url=GLOBAL.ViewChecksDetail;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?PayPeriodId=${PayPeriodId}&EmployeeId=${EmployeeID}&ApplicationId=${ApplicationId}`
        return this.http.get<any>(`${this.url}${queryParams}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
            }));
    }

    AddTimePunches(Timepunchesdetails:any){
        debugger;
        let url=GLOBAL.AddTimePunches;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, Timepunchesdetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            debugger
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }

    updateTimepunches(employeeTimePunchesDetails:any){
        debugger
        let url=GLOBAL.UpdateTimePunches;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        return this.http.post<any>(`${url}`, employeeTimePunchesDetails)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            if (response && response.access_token) {
                console.log(response);
                // this.commonService.setAuth(response);
            } else if (response && response.data) {
                // this.commonService.setSecurityQuestions({ loginResponse: response, authData: postData });
            }
            return response;
        }));
    }

    GetEmployeecheckList(pageNumber: number, pageSize: number){
        this.url=GLOBAL.GetEmployeecheckList;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?page=${pageNumber}&pageSize=${pageSize}`
        return this.http.get<any>(`${this.url}${queryParams}`, )
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    GetEmployeeCheckslistHistory(PayPeriodId: number=0,EmployeeId : number = 0,pageNumber: number, pageSize: number,SearchText:any){
        this.url=GLOBAL.GetEmployeecheckListHistory;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams  = `?page=${pageNumber}&pageSize=${pageSize}&Emp_ID=${EmployeeId}&PayPeriodId=${PayPeriodId}&SearchText=${SearchText}`
        return this.http.get<any>(`${this.url}${queryParams}`, )
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }


    GetChecksList(Emp_ID: number,PayPeriodId : number = 0){
        this.url=GLOBAL.GetChecksList;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?Emp_ID=${Emp_ID}&PayPeriodId=${PayPeriodId}`
        return this.http.get<any>(`${this.url}${queryParams}`, )
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    GetTimePunchHistory(PayPeriodId: number=0,EmployeeId : number = 0,pageNumber: number, pageSize: number,SearchText:any){
        this.url=GLOBAL.GetTimePunchHistoryList;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?page=${pageNumber}&pageSize=${pageSize}&Emp_ID=${EmployeeId}&PayPeriodId=${PayPeriodId}&SearchText=${SearchText}`
        return this.http.get<any>(`${this.url}${queryParams}`, )
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    GetPayPeriodList(){
        this.url=GLOBAL.BindPayPeriodList;
        debugger
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }
    GetEmployeepayperiodList(pageNumber: number, pageSize: number){
        this.url=GLOBAL.GetPayPeriodList;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?page=${pageNumber}&pageSize=${pageSize}`
        return this.http.get<any>(`${this.url}${queryParams}`,)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    GetEmployeePayPeriodlistByName(Seaching:string,pageNumber: number, pageSize: number){
        this.url=GLOBAL.GetPayPeriodListByName;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?Seaching=${Seaching}&page=${pageNumber}&pageSize=${pageSize}`
        return this.http.get<any>(`${this.url}${queryParams}`,)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    Paypayrollprocess(PayrollprocessDetails:any){
        this.url=GLOBAL.PayProllProcess;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        // const queryParams = `?PayperiodId=${PayperiodId}`
        return this.http.post<any>(`${this.url}`,PayrollprocessDetails, )
            .pipe(map(response => {
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    AddCheckIdInTaxDeduction(PayrollprocessDetails:any){
        this.url=GLOBAL.AddCheckIdInTaxDeduction;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        // const queryParams = `?PayperiodId=${PayperiodId}`
        return this.http.post<any>(`${this.url}`,PayrollprocessDetails, )
            .pipe(map(response => {
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    GetTypelist(){
        this.url=GLOBAL.typelist;
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {

            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
    }

    GetEmployeeTimeList(pageNumber: number, pageSize: number,TimePunchCheck:number = 0){
        this.url=GLOBAL.GetEmployeeTimelist;
        // const headers = new HttpHeaders({
        //     'domaintoken': localStorage.getItem('domainToken'),
        //     // 'Authorization': 'Bearer '+localStorage.getItem('accessToken'),
        //     'Authorization': localStorage.getItem("accessToken"),
        // })
        const queryParams = `?TimePunchCheck=${TimePunchCheck}&page=${pageNumber}&pageSize=${pageSize}`
        return this.http.get<any>(`${this.url}${queryParams}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }

    BindActiveEmployeeList(){
            this.url=GLOBAL.BindActiveEmployee;
            return this.http.get<any>(`${this.url}`)
            .pipe(map(response => {
    
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
            }));
    }
    BindPreviousEmployeeList(){
       // debugger
        this.url=GLOBAL.BindPreviousEmployee;
        return this.http.get<any>(`${this.url}`)
        .pipe(map(response => {
debugger
            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }));
}
    ViewEmployeeTimeList(pageNumber: number, pageSize: number){
            this.url=GLOBAL.ViewEmployeeTimelist;
            // const headers = new HttpHeaders({
            //     'domaintoken': localStorage.getItem('domainToken'),
            //     // 'Authorization': 'Bearer '+localStorage.getItem('accessToken'),
            //     'Authorization': localStorage.getItem("accessToken"),
            // })
            const queryParams = `?page=${pageNumber}&pageSize=${pageSize}`
            return this.http.get<any>(`${this.url}${queryParams}`)
                .pipe(map(response => {
        
                    if (response && response.access_token) {
                        console.log(response);
                     
                    } else if (response && response.data) {
                       
                    }
                    return response;
            }));
    }

    FilteredTimepunchesList(pageNumber: number, pageSize: number,EmployeeId: number = 0, PayPeriodId: number = 0){
                this.url=GLOBAL.FilteredTimepunchesList;
                // const headers = new HttpHeaders({
                //     'domaintoken': localStorage.getItem('domainToken'),
                //     // 'Authorization': 'Bearer '+localStorage.getItem('accessToken'),
                //     'Authorization': localStorage.getItem("accessToken"),
                // })
                const queryParams = `?page=${pageNumber}&pageSize=${pageSize}&EmployeeId=${EmployeeId}&PayPeriodId=${PayPeriodId}`
                return this.http.get<any>(`${this.url}${queryParams}`)
                    .pipe(map(response => {
            
                        if (response && response.access_token) {
                            console.log(response);
                         
                        } else if (response && response.data) {
                           
                        }
                        return response;
                }));
    }
    GetIsChangesOnTimePuches(){

        this.url=GLOBAL.GetIsChangesOnTimePuches;
        return this.http.get<any>(`${this.url}`).pipe(map(response=>{
            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }))
    }

    PrintEmplyeeChecks(FromEmployeenop:string,ToemployeeNO:string,EmployeeID:number,PayPeriodId:number,ApplicationId:number=0){
    this.url=GLOBAL.GenerateEmployeeCheck;
    const queryParams = `?FromEmployeeNO=${FromEmployeenop}&ToemployeeNO=${ToemployeeNO}&PayPeriodId=${PayPeriodId}&EmployeeId=${EmployeeID}&ApplicationId=${ApplicationId}`
    const options = { responseType: 'blob', observe: 'response' };

    // return this.http.get<any>(`${this.url}${queryParams}`,{responseType: 'blob'}).pipe(map(response=>{
    //     // if (response && response.access_token) {
    //     //     console.log(response);
         
    //     // } else if (response && response.data) {
           
    //     // }
    //     // return response;
    // }))
        return this.http.get(`${this.url}${queryParams}`, { responseType: 'blob' });
    }


    closepayrollprocess(PayrollprocessDetails:any){
        this.url=GLOBAL.Closepayrollprocess;
        return this.http.post<any>(`${this.url}`,PayrollprocessDetails, )
            .pipe(map(response => {
                if (response && response.access_token) {
                    console.log(response);
                 
                } else if (response && response.data) {
                   
                }
                return response;
        }));
    }


    getcurrentFromemployeelist(FromEmployeenop,seletedToEmployeeId){
        this.url=GLOBAL.FromEmployeeList;
        const queryParams = `?EmployeeNo=${FromEmployeenop}&EmployeeID=${seletedToEmployeeId}`
        return this.http.get<any>(`${this.url}${queryParams}`).pipe(map(response=>{
            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }))
    }

    getcurrentToemployeelist(ToEmployeenop,seletedFromEmployeeId){
        this.url=GLOBAL.ToEmployeeList;
        const queryParams = `?EmployeeNo=${ToEmployeenop}&EmployeeID=${seletedFromEmployeeId}`
        return this.http.get<any>(`${this.url}${queryParams}`).pipe(map(response=>{
            if (response && response.access_token) {
                console.log(response);
             
            } else if (response && response.data) {
               
            }
            return response;
        }))
    }
}
