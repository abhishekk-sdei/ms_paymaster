import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { inject } from '@angular/core/testing';

@Injectable()
export class CommonService {
    
    
    constructor(private http: HttpClient,) { }


    // public Auth =localStorage && localStorage.getItem('accessToken').length>1?'Bearer '+localStorage.getItem('accessToken'):"";
    public Auth :any;


    Get(url): Observable<any> {
        // const headers = new HttpHeaders({ additionalHeaders: this.getAdditionalHeaders });
            return this.http.get<any>(`${url}`).pipe(map(res => {
                return res;
            }));
    }

    
}