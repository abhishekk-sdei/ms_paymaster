import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SubdomainService } from './subdomain.service';
import { take } from 'rxjs/internal/operators/take';

@Injectable({
  providedIn: 'root'
})
export class SubdomainGuard implements CanActivate {

  constructor(private router: Router,
    private subDomainService: SubdomainService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
      if (this.subDomainService.GetSubDomainUrl()) {
         return this.subDomainService.isValidDomain.pipe(take(1));
        return true;

    }
    
  
  }
  
}
