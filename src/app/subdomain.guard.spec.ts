import { TestBed, async, inject } from '@angular/core/testing';

import { SubdomainGuard } from './subdomain.guard';

describe('SubdomainGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SubdomainGuard]
    });
  });

  it('should ...', inject([SubdomainGuard], (guard: SubdomainGuard) => {
    expect(guard).toBeTruthy();
  }));
});
