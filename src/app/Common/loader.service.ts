import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
 

  private loaderSubject = new BehaviorSubject<LoaderState>({ show: false });
  loaderState = this.loaderSubject.asObservable();
  constructor() { }
  show() {
    console.log("on");
    this.loaderSubject.next(<LoaderState>{ show: true });
  }
  hide() {
    console.log("off");
    this.loaderSubject.next(<LoaderState>{ show: false });
  }
}
export interface LoaderState {
  show: boolean;
}