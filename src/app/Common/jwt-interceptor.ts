import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        if (localStorage.getItem('domainToken')) {
            request = request.clone({
                setHeaders: { 
                    'domaintoken': localStorage.getItem('domainToken')
                }
            });
        }
        if (localStorage.getItem('accessToken')) {
            request = request.clone({
                setHeaders: { 
                    'Authorization': localStorage.getItem('accessToken')
                }
            });
        }

        return next.handle(request);
    }
}