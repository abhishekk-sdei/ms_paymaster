import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthGuard implements CanActivate {

  constructor(private router:Router ){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){
      debugger
      if (localStorage.getItem("accessToken").length>1) {
        //  return this.subDomainService.isValidDomain.pipe(take(1));
        return true;
     }else{
      this.router.navigate(['/']);
     }
    // return true;
  }
  
}
