import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../Shared/shared.module';
import {organizationportalRoutingModule} from '../organization-portal/organization-portal-Routing';
import { LoginComponent } from './login/login.component';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmployeeComponent } from './Employees/employee/employee.component';
import { AddUpdateEmployeeComponent } from './Employees/add-update-employee/add-update-employee.component';
import { AuthenticationService } from '../Services/AuthenticationService';
import { EmployeeService } from '../Services/EmployeeServices';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatPaginatorModule, MatSortModule, MatDialogModule } from '@angular/material';
import { TimePunchesComponent } from './Employees/time-punches/time-punches.component';
import {MatSelectModule} from '@angular/material/select';
import { CommonNumariceDirectiveDirective } from './common-directive.directive';
import { EditEmployeeComponent } from './Employees/edit-employee/edit-employee.component';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { MatTableModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatProgressSpinnerModule } from '@angular/material';
import { ViewEmployeeTimePunchesComponent } from './Employees/view-employee-time-punches/view-employee-time-punches.component'
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { from } from 'rxjs';
import {MatDialog, MatDialogConfig} from "@angular/material";
import { ViewEmployeedetailComponent } from './Employees/view-employeedetail/view-employeedetail.component'
import {OnlyNumericDirective} from 'src/app/Directive/only-numeric.directive';
import { ForgetPasswordComponent } from './login/forget-password/forget-password.component';
import { ResetPasswordComponent } from './login/reset-password/reset-password.component';
import { ChecksComponent } from './Employees/checks/checks.component';
import { PayperiodComponent } from './Employees/payperiod/payperiod.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ViewChecksDetailComponent } from './Employees/view-checks-detail/view-checks-detail.component';
import { CommonService } from '../Services/Commonservices';
import { JwtInterceptor } from '../Common/jwt-interceptor';
// import { ViewListDetailComponent } from './Employees/view-list-detail/view-list-detail.component';
import { ViewListDetailComponent } from './Employees/view-list-detail/view-list-detail.component';
import { ViewTimePunchesHistoryComponent } from './Employees/view-time-punches-history/view-time-punches-history.component';
import { AddEmployeePayrollComponent } from './Employees/add-employee-payroll/add-employee-payroll.component';
import {MatStepperModule} from '@angular/material/stepper';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ViewChecksHistoryComponent } from './Employees/view-checks-history/view-checks-history.component';
import { MatRadioModule } from '@angular/material/radio';
import { LoaderInterceptorService } from '../Common/loader-interceptor.service';
// import { ProcessPayrollComponent } from './Employees/process-payroll/process-payroll.component';

@NgModule({
  declarations: [DefaultLayoutComponent,
    LoginComponent,
    DashboardComponent,
    EmployeeComponent,
    AddUpdateEmployeeComponent,
    TimePunchesComponent,
    CommonNumariceDirectiveDirective,
    EditEmployeeComponent, 
    ViewEmployeeTimePunchesComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    ViewEmployeedetailComponent,OnlyNumericDirective, ChecksComponent, PayperiodComponent, LandingPageComponent, SignUpComponent, ViewChecksDetailComponent,ViewListDetailComponent, ViewTimePunchesHistoryComponent, AddEmployeePayrollComponent, ViewChecksHistoryComponent],
    //ViewEmployeedetailComponent,OnlyNumericDirective, ChecksComponent, PayperiodComponent, LandingPageComponent, SignUpComponent, ViewListDetailComponent],
  imports: [
    SharedModule,
    MatPaginatorModule,
    MatSelectModule,
    NgxMaterialTimepickerModule,
    CommonModule,
    organizationportalRoutingModule,
    HttpClientModule,
    MatTableModule,
    MatSortModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    MatStepperModule,
     AngularFontAwesomeModule,
    MatRadioModule,
    ToastrModule.forRoot()  
  ],providers: [AuthenticationService,EmployeeService,CommonService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true }],
  entryComponents: [ViewEmployeeTimePunchesComponent],
  exports: [
    SharedModule,
  ]
})
export class OrganizationPortalModule { }
