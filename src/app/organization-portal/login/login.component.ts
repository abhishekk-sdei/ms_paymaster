import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/Services/AuthenticationService';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/Services/Commonservices';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  LoginForm: FormGroup;
  submitted: boolean;
  constructor(private toastr: ToastrService,private authenticationService: AuthenticationService,private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,private commonService: CommonService) { }
   
  ngOnInit() {
    this.LoginForm = this.formBuilder.group({
      Username: ['', [Validators.required]],
      Password: ['', [Validators.required]]
    });
  }
  get LoginDetails() { return this.LoginForm.controls; }
  
  OnFormSubmit(loginViewModel:any){
    this.submitted = true;
    debugger;
    if(this.LoginForm.valid){
      const postData = {
        Username: this.LoginDetails.Username.value,
        Password: this.LoginDetails.Password.value,
      };
      this.authenticationService.organizationlogin(postData).subscribe((response => {
        // login successful if there's a jwt token in the response
        if (response.statusCode==200) {
              if(response.value.token.length>0){
                localStorage.setItem("accessToken","Bearer "+response.value.token);
                this.commonService.Auth=localStorage.getItem("accessToken");
              localStorage.setItem("UserID", response.value.userId);
              this.router.navigate(['/home/employee']);
            }
            else{
             if( response.value.statusCode==400) {
             this.toastr.error(response.value.message);}
           }
        } 
      return response;}))
    }
  }
}
