import { Component, OnInit,Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { Router } from '@angular/router';
import { MatSelect } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import 'moment/locale/pt-br';
//import {OnlyNumberServices} from 'src/app/Services/OnlyNumberServices';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  OrganizationFrom : FormGroup;
  CountryList : any;
  constructor(private toastr: ToastrService,private formBuilder: FormBuilder,private emplyeeService:EmployeeService,private router: Router) { }

  ngOnInit() {
    this.OrganizationFrom = this.formBuilder.group({
      //EmployeeNo: ['', [Validators.required]],
  
      CompanyName: ['', [Validators.required]],
      CompanyEmail: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      CompanyContactNumber: ['', [Validators.required]],
      Password: ['', [Validators.required]],
      Country: ['',  [Validators.required]],

    });
    this.emplyeeService.GetCountryList().subscribe((res=>{
      if(res){
        this.CountryList=res||[];
        // console.log(res);
      }else{

      }
    }));
  //   if(localStorage.getItem("accessToken").length>1)
  //   {
   
    
  // }
  // else{
  //   this.router.navigate(['/']);
  // }
}

get OrganizationDetails() { return this.OrganizationFrom.controls; }
OnformSubmit(){
    
    //this.submitted = true;
    debugger;
    //alert(i);
   
      if(this.OrganizationFrom.valid){
        debugger;
       
        const Organizationdata={
          
        //Number: this.EmployeeDetails.EmployeeNo.value,
        OrganizationEmail : this.OrganizationDetails.CompanyEmail.value,
        OrganizationName : this.OrganizationDetails.CompanyName.value,
        OrganizationContactNumber : this.OrganizationDetails.CompanyContactNumber.value,
        Password : this.OrganizationDetails.Password.value,
        Country : this.OrganizationDetails.Country.value,
        }
        debugger;
        this.emplyeeService.AddOrganization(Organizationdata).subscribe((res=>{
          if (res) {
            debugger
            if(res.statusCode === 200)
            {
            this.toastr.success(res.message);
          }
          else
          {
            this.toastr.error(res.message);
          }
            // localStorage.setItem("accessToken", res.value.token);
              //this.router.navigate(['/home/employee']);
           } 
        }))
      
    }

    
  }


}
