import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import{OrganizationPortalComponent} from './organization-portal.component';
// import { OrganizationPortalModule } from './organization-portal.module';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmployeeComponent } from './Employees/employee/employee.component';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';
import { LoginAuthGuard } from './gaurds/Login/login-auth.guard';
import { AddUpdateEmployeeComponent } from './Employees/add-update-employee/add-update-employee.component';
import { TimePunchesComponent } from './Employees/time-punches/time-punches.component';
import { EditEmployeeComponent } from './Employees/edit-employee/edit-employee.component';
import { ViewEmployeeTimePunchesComponent } from './Employees/view-employee-time-punches/view-employee-time-punches.component';
import { ViewEmployeedetailComponent } from './Employees/view-employeedetail/view-employeedetail.component';
import { ForgetPasswordComponent } from './login/forget-password/forget-password.component';
import { ResetPasswordComponent } from './login/reset-password/reset-password.component';
import { ChecksComponent } from './Employees/checks/checks.component';
import { PayperiodComponent } from './Employees/payperiod/payperiod.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ViewChecksDetailComponent } from './Employees/view-checks-detail/view-checks-detail.component';
import { SubdomainGuard } from '../subDomain.guard';
import { ViewListDetailComponent } from './Employees/view-list-detail/view-list-detail.component';
import { ViewTimePunchesHistoryComponent } from './Employees/view-time-punches-history/view-time-punches-history.component';
import { AddEmployeePayrollComponent } from './Employees/add-employee-payroll/add-employee-payroll.component';
import { ViewChecksHistoryComponent } from './Employees/view-checks-history/view-checks-history.component';
const routes: Routes = [
    {
        path: 'signIn', 
        canActivate:[SubdomainGuard],
        component: LoginComponent
    },
    {
      path: '',
      component: LandingPageComponent,
    },
    {
      path: 'viewlist-detail',
      component: ViewListDetailComponent,
    },
    {
      path: 'signUp',
      component: SignUpComponent,
    },
    {
      path: 'forgetPassword',
      component: ForgetPasswordComponent,
  },
  {
    path: 'resetPassword',
    component: ResetPasswordComponent,
  },
    {
      path: 'home',
      component:DefaultLayoutComponent,
      
      children: [
        {
          path: '',
          component: DashboardComponent
        },
        {
          path:'employee',
           canActivate:[LoginAuthGuard],
          component:EmployeeComponent,
          data:{
            title:'employee'
          }
        },
        {
          path:'add-employee',
           canActivate:[LoginAuthGuard],
          component:AddUpdateEmployeeComponent,
          data:{
            title:'AddEmployee'
          }
        },
        {
          path:'Edit-employee',
           canActivate:[LoginAuthGuard],
          component:EditEmployeeComponent,
          data:{
            title:'AddEmployee'
          }
        },
        {
          path:'TimePunches',
          canActivate:[LoginAuthGuard],
          component:TimePunchesComponent,
          data:{
            title:'TimePunches'
          }
        },
        {
          path:'ViewTimePunchesHistory',
          canActivate:[LoginAuthGuard],
          component:ViewTimePunchesHistoryComponent,
          data:{
            title:'TimePunchesHistory'
          }
        },
        {
          path:'View-employeeTimePunches',
            canActivate:[LoginAuthGuard],
          component:ViewEmployeeTimePunchesComponent,
          data:{
            title:'View-employeeTimePunches'
          }
        },{
          path:'view-employeedetail',
          canActivate:[LoginAuthGuard],
          component:ViewEmployeedetailComponent,
          data:{
            title:'ViewEmployeeDetail'
          }
        },
        {
          path:'employeechecks',
          canActivate:[LoginAuthGuard],
          component:ChecksComponent,
          data:{
            title:'ViewEmployeeChecklist'
          }
        }
        ,
        {
          path:'PayPeriod',
          component:PayperiodComponent,
          data:{
            title:'PayPeriod'
          }
        }
        ,
        {
          path:'ViewCheckDetail',
          component:ViewChecksDetailComponent,
          data:{
            title:'ViewCheckDetail'
          }
        }
        ,
        {
          path:'addEmployeePayroll',
          component:AddEmployeePayrollComponent,
          data:{
            title:'addEmployeePayroll'
          }
        }
        ,
        {
          path:'view-checks-history',
          component:ViewChecksHistoryComponent,
          data:{
            title:'view-checks-history'
          }
        }
      ]
    }
  
  ];



  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class organizationportalRoutingModule { }