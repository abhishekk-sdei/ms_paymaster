import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { Observable, timer } from 'rxjs';
import { map, switchMap  } from 'rxjs/operators';

import { EmployeeService } from 'src/app/Services/EmployeeServices';

@Injectable({ providedIn: 'root' })
export class EmployeeValidators {
    constructor(private http: HttpClient,private EmpServices:EmployeeService,) {}

    SocSecExist(): AsyncValidatorFn {
            return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
                debugger
                var empid=localStorage.getItem("CurrentEmpid");
            return this.EmpServices.SocSecNoExistOrnot(control.value,empid)
                .pipe(
                map(res => {
                    // if username is already taken
                    if (res.statusCode==409) {
                    // return error
                    return { 'SocsecExists': true};
                    }
                })
                );
            };

    }

}