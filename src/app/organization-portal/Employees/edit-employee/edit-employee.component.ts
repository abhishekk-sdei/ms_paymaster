import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSelect } from '@angular/material';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { ToastrService } from 'ngx-toastr';
import { EmployeeValidators } from '../Validations/user.validator';
@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {
  EmployeeFrom: FormGroup;
  EmployeeBasicFrom : FormGroup;
  EmployeeSalaryFrom : FormGroup;
  EmployeePersonalFrom : FormGroup;
  EmployeePaymentFrom : FormGroup;
  submitted: boolean;
  CountryList: any;
  GenderList: any;
  PayFrequencyList : any;
  RaceList: any;
  MaritalList: any;
  CityList: any;
  Statelist: any;
  TempEmpId :number;
  cid: any;
  sid: any;
  EmpId: any;maxDate=new Date();
  constructor(private Empvalidate:EmployeeValidators,private toastr: ToastrService,private formBuilder: FormBuilder,private emplyeeService:EmployeeService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1)
    {
      // this.EmployeeFrom = this.formBuilder.group({
      //   EmployeeId: ['', [Validators.required]],
      //   EmployeeNo: ['', [Validators.required]],
      //   //Socsec: ['', Validators.compose([Validators.required]),this.Empvalidate.SocSecExist()],
      //   Socsec: ['', Validators.compose([Validators.required]),],
      //   FirstName: ['', [Validators.required]],
      //   MiddleName: ['', ],
      //   LastName: ['', [Validators.required]],
      //   Gender: ['', [Validators.required]],
      //   Race: ['', [Validators.required]],
      //   Maritalstatus: ['', [Validators.required]],
      //   Birthday: ['', [Validators.required]],
      //   Country: ['', [Validators.required]],
      //   State: ['', [Validators.required]],
      //   City: ['', [Validators.required]],
      //   HourlyPayRate: ['', [Validators.required]],
      // });
      this.EmployeePersonalFrom = this.formBuilder.group({
        //EmployeeNo: ['', [Validators.required]],
        EmployeeId: ['', [Validators.required]],
        Country: ['', [Validators.required]],
        State: ['', [Validators.required]],
        City: ['', [Validators.required]],
        HeadOfHousehold: ['',],
        Blind: ['', ],
        Student: ['',],
  
      });
      this.EmployeeSalaryFrom = this.formBuilder.group({
        //EmployeeNo: ['', [Validators.required]],
    
        EmployeeId: ['', [Validators.required]],
        HourlyPayRate: ['', [Validators.required]],
        Hours: ['', [Validators.required]],
        PayPeriod: ['', [Validators.required]],
  
        EarnedIncomeCredit: ['', ],
        EarnedIncomeCreditChildren: ['', ],
        Allowances: ['',],
  
        Dependents: ['', ],
        Exemptions: ['',],
        FlatFit: ['', ],
        AdditionalWitholdingAmount: ['',],
        //PartTime: ['',],
  
        
  
      });
      this.EmployeeBasicFrom = this.formBuilder.group({
        //EmployeeNo: ['', [Validators.required]],
        EmployeeId: ['', [Validators.required]],
        Socsec: ['', Validators.compose([Validators.required]),],
        FirstName: ['', [Validators.required]],
        MiddleName: ['',],
        LastName: ['', [Validators.required]],
        Gender: ['', [Validators.required]],
        Race: ['', [Validators.required]],
        Maritalstatus: ['', [Validators.required]],
        Birthday: ['', [Validators.required]],
        Hiredate: ['', [Validators.required]],
  
      });
      this.route.queryParams.subscribe(params => {
        debugger;
        if (params['EmpId'].length> 0) {
          
          this.EmpId = localStorage.getItem("editemployeeid");
          localStorage.setItem("CurrentEmpid", this.EmpId.toString())
            this.emplyeeService.GetEmployeeById(this.EmpId).subscribe((res=>{
              console.log(res);
              debugger; 
              this.cid=res.data['countryId'];
              this.sid=res.data['stateId'];
              // this.EmployeeFrom.setValue({
              //   // EmployeeId:res.data['employeeID'],
              //   EmployeeId:res.data['enyEmployeeID'],
              //   EmployeeNo:res.data['number'],
              //   Socsec:res.data['socSec'],
              //   FirstName:res.data['firstName'],
              //   MiddleName:res.data['middleName'],
              //   LastName:res.data['lastName'],
              //   Gender:res.data['sex'],
              //   Race:res.data['race'],
              //   Maritalstatus:res.data['maritalStatus'],
              //   Birthday:res.data['birthDate'],
              //   Country:res.data['countryId'],
              //   State:res.data['stateId'],
              //   City:res.data['cityName'],
              //   HourlyPayRate:res.data['hourlyPayRate'],
              //   Hours : res.data['hours'],
              //   EarnedIncomeCredit : res.data['earnedIncomeCredit'],
              //   EarnedIncomeCreditChildren : res.data['earnedIncomeCreditChildren'],
              //   FlatFit : res.data['flatFit'],
              //   AdditionalWitholdingAmount : res.data['additionalWitholdingAmount'],
              //   HeadOfHousehold : res.data['headOfHousehold'],
              //   Blind : res.data['blind'],
              //   Student : res.data['student'],
              //   Allowances : res.data['allowances'],
              //   Dependents : res.data['dependents'],
              //   Exemptions : res.data['exemptions'],
              //   PayFrequency : res.data['payFrequency'],
              //   Hiredate:res.data['hiredate'],
              // });
              this.EmployeeBasicFrom.setValue({
                // EmployeeId:res.data['employeeID'],
                //EmployeeId:res.data['enyEmployeeID'],
                //EmployeeNo:res.data['number'],
                EmployeeId:res.data['enyEmployeeID'],
                Socsec:res.data['socSec'],
                FirstName:res.data['firstName'],
                MiddleName:res.data['middleName'],
                LastName:res.data['lastName'],
                Gender:res.data['sex'],
                Race:res.data['race'],
                Maritalstatus:res.data['maritalStatus'],
                Birthday:res.data['birthDate'],
                Hiredate:res.data['hiredate'],
              });
              this.EmployeePersonalFrom.setValue({
                // EmployeeId:res.data['employeeID'],
                //EmployeeId:res.data['enyEmployeeID'],
                EmployeeId:res.data['enyEmployeeID'],
                Country:res.data['countryId'],
                State:res.data['stateId'],
                City:res.data['cityName'],
                HeadOfHousehold : res.data['headOfHousehold'] == true ? '1':'2',
                Blind : res.data['blind'],
                Student : res.data['student'] == true ? '1':'2',
              });
              this.EmployeeSalaryFrom.setValue({
                // EmployeeId:res.data['employeeID'],
                //EmployeeId:res.data['enyEmployeeID'],
                HourlyPayRate:res.data['hourlyPayRate'],
                EmployeeId:res.data['enyEmployeeID'],
                Hours : res.data['hours'],
                EarnedIncomeCredit : res.data['earnedIncomeCredit'],
                EarnedIncomeCreditChildren : res.data['earnedIncomeCreditChildren'],
                FlatFit : res.data['flatFit'],
                AdditionalWitholdingAmount : res.data['additionalWitholdingAmount'],          
                Allowances : res.data['allowances'],
                Dependents : res.data['dependents'],
                Exemptions : res.data['exemptions'],
                PayPeriod : res.data['payFrequency'],
              });
              this.getstateList(this.cid);
              this.getCityList(this.cid,this.sid);
            }))
        }
      });
      
      this.emplyeeService.GetGenderList().subscribe((res=>{
        if(res){
          this.GenderList=res||[];
          // console.log(res);
        }else{

        }
      }));
      this.emplyeeService.GetPayFrequencyList().subscribe((res=>{
        if(res){
          this.PayFrequencyList=res||[];
          // console.log(res);
        }else{
  
        }
      }));
      this.emplyeeService.GetRaceList().subscribe((res=>{
        if(res){
          this.RaceList=res||[];
          // console.log(res);
        }else{

        }
      }));

      this.emplyeeService.GetMaritalList().subscribe((res=>{
        if(res){
          this.MaritalList=res||[];
        }else{

        }
      }));

      this.emplyeeService.GetCountryList().subscribe((res=>{
        if(res){
          this.CountryList=res||[];
          // console.log(res);
        }else{

        }
      }));
    }
  else{
    this.router.navigate(['/']);
  }
  }
 // get EmployeeDetails() { return this.EmployeeFrom.controls; }
 get EmployeeBasicDetails() { return this.EmployeeBasicFrom.controls; }
 get EmployeeSalaryDetails() { return this.EmployeeSalaryFrom.controls; }
 get EmployeePersonalDetails() { return this.EmployeePersonalFrom.controls; }

 

  getstateList(CountryId:number){
    this.emplyeeService.GetStateList(CountryId).subscribe((res=>{
      if(res){
        this.Statelist=res||[];
        console.log(res);
      }else{
  
      }
    }));
  
  }
  

  getCityList(CityId:number,stateId:number){
    this.emplyeeService.GetCityList(CityId,stateId).subscribe((res=>{
      if(res){
        this.CityList=res||[];
        // console.log(res);
      }else{
  
      }
    }));
  }
  
  OnStateSelect(event:MatSelect){
    debugger;
    let StateID = event.value;
    this.getCityList(this.cid,StateID);
  }

  OnCountrySelect(event:MatSelect){
    debugger;
    let CountryId = event.value;
    this.cid=CountryId
    this.getstateList(this.cid);
  }
  OnEmpformSubmit(i){
    this.submitted = true;
    debugger;
    if(i == 1)
    {
      if(this.EmployeeBasicFrom.valid){
        debugger;
       
        const EmpBasicdata={
          
        //Number: this.EmployeeDetails.EmployeeNo.value,
           // EmployeeID:this.EmployeeDetails.EmployeeId.value,
        EnyEmployeeID:this.EmployeeBasicDetails.EmployeeId.value,
        Socsec:this.EmployeeBasicDetails.Socsec.value ,
        FirstName: this.EmployeeBasicDetails.FirstName.value,
        MiddleName: this.EmployeeBasicDetails.MiddleName.value,
        LastName:this.EmployeeBasicDetails.LastName.value,
        Sex: this.EmployeeBasicDetails.Gender.value,
        Race: this.EmployeeBasicDetails.Race.value,
        Maritalstatus: this.EmployeeBasicDetails.Maritalstatus.value,
        BirthDate: moment(this.EmployeeBasicDetails.Birthday.value).format("YYYY-MM-DD"),
        Hiredate : moment(this.EmployeeBasicDetails.Hiredate.value).format("YYYY-MM-DD"),
        UserId:parseInt(localStorage.getItem("UserID")),
        CreatedBy:parseInt(localStorage.getItem("UserID"))
        }
        debugger;
        this.emplyeeService.UpdateEmployeeBasicDetails(EmpBasicdata).subscribe((res=>{
          if (res) {
            debugger
            this.TempEmpId = res.data;
            this.toastr.success(res.message);
            // localStorage.setItem("accessToken", res.value.token);
              //this.router.navigate(['/home/employee']);
           } 
        }))
      }
    }
    else if(i == 2)
    {
      if(this.EmployeeSalaryFrom.valid){
        debugger;
       
        const EmpSalarydata={
          
          EnyEmployeeID:this.EmployeeSalaryDetails.EmployeeId.value,
          PayFrequency : this.EmployeeSalaryDetails.PayPeriod.value,
        HourlyPayRate: parseFloat(this.EmployeeSalaryDetails.HourlyPayRate.value),
        EarnedIncomeCredit  : parseInt(this.EmployeeSalaryDetails.EarnedIncomeCredit.value),
        EarnedIncomeCreditChildren:parseInt(this.EmployeeSalaryDetails.EarnedIncomeCreditChildren.value),
        Allowances: parseInt(this.EmployeeSalaryDetails.Allowances.value),
        Dependents: parseInt(this.EmployeeSalaryDetails.Dependents.value),
        Exemptions: parseInt(this.EmployeeSalaryDetails.Exemptions.value),
        FlatFit: parseFloat(this.EmployeeSalaryDetails.FlatFit.value),
        AdditionalWitholdingAmount : parseFloat(this.EmployeeSalaryDetails.AdditionalWitholdingAmount.value),    
        Hours: parseFloat(this.EmployeeSalaryDetails.Hours.value),
        //UserId:parseInt(localStorage.getItem("UserID")),
        //CreatedBy:parseInt(localStorage.getItem("UserID"))
        }
        debugger;
        this.emplyeeService.UpdateEmployeeSalaryDetails(EmpSalarydata).subscribe((res=>{
          if (res) {
            debugger
            this.toastr.success(res.message);
            // localStorage.setItem("accessToken", res.value.token);
              //this.router.navigate(['/home/employee']);
           } 
        }))
      }
    }
    else if(i == 3)
    {
      if(this.EmployeePersonalFrom.valid){
        debugger;
       
        const EmpPersonaldata={
          EnyEmployeeID:this.EmployeePersonalDetails.EmployeeId.value,
          CountryId:this.EmployeePersonalDetails.Country.value,
          StateId:this.EmployeePersonalDetails.State.value,
          CityId: this.EmployeePersonalDetails.City.value,
          HeadOfHousehold : parseInt(this.EmployeePersonalDetails.HeadOfHousehold.value) == 1,
          Blind : this.EmployeePersonalDetails.Blind.value,
          Student :  parseInt(this.EmployeePersonalDetails.Student.value) == 1,
        //UserId:parseInt(localStorage.getItem("UserID")),
        //CreatedBy:parseInt(localStorage.getItem("UserID"))
        }
        debugger;
        this.emplyeeService.UpdateEmployeePersonalDetails(EmpPersonaldata).subscribe((res=>{
          if (res) {
            debugger
            this.toastr.success(res.message);
            // localStorage.setItem("accessToken", res.value.token);
              //this.router.navigate(['/home/employee']);
           } 
        }))
      }
    } 
    else if(i == 4)
    {
      
    }

    // if(this.EmployeeFrom.valid){
      
    //   const Empdata={
    //   // EmployeeID:this.EmployeeDetails.EmployeeId.value,
    //   EmployeeID:0,
    //   EnyEmployeeID:this.EmployeeDetails.EmployeeId.value,
    //   Number: this.EmployeeDetails.EmployeeNo.value,
    //   Socsec:this.EmployeeDetails.Socsec.value ,
    //   FirstName: this.EmployeeDetails.FirstName.value,
    //   MiddleName: this.EmployeeDetails.MiddleName.value,
    //   LastName:this.EmployeeDetails.LastName.value,
    //   Sex: this.EmployeeDetails.Gender.value,
    //   Race: this.EmployeeDetails.Race.value,
    //   Maritalstatus: this.EmployeeDetails.Maritalstatus.value,
    //   BirthDate: moment(this.EmployeeDetails.Birthday.value).format("YYYY-MM-DD"),
    //   CountryId:this.EmployeeDetails.Country.value,
    //   StateId:this.EmployeeDetails.State.value,
    //   CityId: this.EmployeeDetails.City.value,
    //   HourlyPayRate: parseFloat(parseFloat(this.EmployeeDetails.HourlyPayRate.value).toFixed(2)),
    //   }
      
    //   this.emplyeeService.UpdateEmployee(Empdata).subscribe((res=>{
    //     if (res) {
    //        this.toastr.success(res.message);
    //       // localStorage.setItem("accessToken", res.value.token);
    //          this.router.navigate(['/home/employee']);
    //      } 
    //   }))
    // }
  }


  
}
