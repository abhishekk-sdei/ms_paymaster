import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import {PageEvent} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { ToastrService } from 'ngx-toastr';
import {MatDialog, MatDialogConfig} from "@angular/material";
import { ViewEmployeeTimePunchesComponent } from '../view-employee-time-punches/view-employee-time-punches.component';
import { ViewEmployeedetailComponent } from '../view-employeedetail/view-employeedetail.component';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})

export class EmployeeComponent implements OnInit {
  
  pageNumber: number = 1;
  pageSize:number = 10;
  totalRecords: number;
  totalPages:number;
  display:any = 'none';
  deleteEmployeeId : number;
  employeelist=[];
  EmployeeData:EmployeeDetails[];
  // displayedColumns: string[] = ['EMPLOYEENUMBER','EMPLOYEENAME', 'SOCSEC', 'JOININGDATE','HOURLYPAYRATE', 'TOTALAMOUNT','HOURS','ACTION'];
  displayedColumns: string[] = ['EMPLOYEENUMBER','EMPLOYEENAME', 'SOCSEC', 'JOININGDATE','HOURLYPAYRATE','ACTION'];
  EmployeeSource = new MatTableDataSource(this.EmployeeData);
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  noData: any;
  
  constructor(private router:Router,private dialog: MatDialog,private EmployeeServices :EmployeeService,private toastr: ToastrService,) { }
  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1){
    // this.EmployeeSource.sort = this.sort;
    this.GetEmployeeList();}
    else{
      this.router.navigate(['/']);
    }
  }

  ngAfterViewInit() {
   
    this.EmployeeSource.sort = this.sort;
  }
  GetEmployeeList() {
    this.EmployeeServices.GetEmployeeList(this.pageNumber, this.pageSize).subscribe((res=>{
      if(res)
      {
        let Employeearray = [];
       
        debugger;
        if(res.data.length>0)
        {
        this.employeelist=res.data;
        
        this.employeelist.forEach(function(value){
          let employeedata={
          //EMPLOYEEID:value['employeeID'],
          // EMPLOYEEID:value['employeeID'],
          EncyEMPLOYEEID:value['enctypedEmployeeID'],
          EMPLOYEEID:value['employeeID'],
          EMPLOYEENAME: 
          value['firstName'] +
          ((value['middleName'] ===undefined || value['middleName'] === null)?'':' '+value['middleName']) 
          +
          ((value['lastName'] ===undefined || value['lastName'] === null)?'':' '+value['lastName']) ,
          EMPLOYEENUMBER:value['number'],
          JOININGDATE:value['createdDate'],
          SOCSEC:value['socSec'],
          // HOURLYPAYRATE:value['hourlyPayRate'].toFixed(2),
          HOURLYPAYRATE:parseFloat(value['hourly_PayRate']).toFixed(2),
          // TOTALAMOUNT:value['basicSalary'],
          // HOURS:value['totalTime'].toFixed(2),
          ACTION:''
        }
        Employeearray.push(employeedata);
        })
        this.EmployeeData=Employeearray;
        
        this.totalRecords=parseInt(res.meta["totalRecords"]);
        this.totalPages=parseInt(res.meta["totalPages"]);
        
        // console.log(res,this.EmployeeSource);
        this.EmployeeSource.data = this.EmployeeData
       
        }
        else{
          this.EmployeeSource.data=Employeearray;
          this.noData = this.EmployeeSource.connect().pipe(map(data => data.length === 0));
        }
    }
      else{
        
      }return res;}))
  }

  Edit(element){
    debugger
    localStorage.setItem("editemployeeid",element.EMPLOYEEID);
    this.router.navigate(['/home/Edit-employee'], { queryParams: { EmpId: element.EncyEMPLOYEEID } });
  }
  DeleteModal(EMPLOYEEID,_e)
  {
    debugger
   // this.display='block';
    this.deleteEmployeeId = EMPLOYEEID;
  }
  DeleteEmployee(event){
    debugger;
    this.EmployeeServices.Deleted(this.deleteEmployeeId ).subscribe((res=>{
      if(res.statusCode==200){
        this.toastr.success(res.message);
        //this.display='none'; 
        this.GetEmployeeList();
      }
    }))
  }

  GetEmployeeListByName(event:any){
    debugger;
    if(event.target.value=="")
    {
      this.GetEmployeeList();
    }
    else{
      this.EmployeeServices.GetEmployeeListByName(event.target.value, 1, this.pageSize = 10).subscribe((res=>{
        if(res)
        {
         
          debugger;
          let Employeearray = [];
          
            if(res.data.length>0){

            this.employeelist=res.data;
            this.employeelist.forEach(function(value){
              let employeedata={
              //EMPLOYEEID:value['employeeID'],
              // EMPLOYEEID:value['employeeID'],
              EncyEMPLOYEEID:value['enctypedEmployeeID'],
              EMPLOYEEID:value['employeeID'],
              EMPLOYEENAME: 
              value['firstName'] +
              ((value['middleName'] ===undefined || value['middleName'] === null)?'':' '+value['middleName']) 
              +
              ((value['lastName'] ===undefined || value['lastName'] === null)?'':' '+value['lastName']) ,
              EMPLOYEENUMBER:value['number'],
              JOININGDATE:value['createdDate'],
              SOCSEC:value['socSec'],
              // HOURLYPAYRATE:value['hourlyPayRate'].toFixed(2),
              HOURLYPAYRATE:parseFloat(value['hourly_PayRate']).toFixed(2),
              // TOTALAMOUNT:value['basicSalary'],
              // HOURS:value['totalTime'].toFixed(2),
              ACTION:''
            }
            Employeearray.push(employeedata);
            })
            this.EmployeeData=Employeearray;
            
            this.totalRecords=parseInt(res.meta["totalRecords"]);
            this.totalPages=parseInt(res.meta["totalPages"]);
            
            console.log(res,this.EmployeeSource);
            this.EmployeeSource.data = this.EmployeeData
          }
          else{
            this.EmployeeSource.data=Employeearray;
            this.noData = this.EmployeeSource.connect().pipe(map(data => data.length === 0));
            this.totalRecords=0;
            this.totalPages=0;
          }
        
          
        }
        else{
          
        }return res;
      }))
    }
    
  }
  openDialog(EmployeeID:string) {

    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data={
      EmployeeId:EmployeeID
    }

   
    const dialogRef =this.dialog.open(ViewEmployeeTimePunchesComponent,dialogConfig);

    dialogRef.afterClosed().subscribe(confirmresult=>{ 
      debugger 
       
       this.GetEmployeeList();

       
      // if(confirmresult){
      //   debugger            //if dialog result is yes, delete post  
      //   this.GetEmployeeList();
      // }  
       
    })  
   // return test;
}
openDialogViewEmpDetail(EMPLOYEEID) {
debugger
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = false; 
  dialogConfig.autoFocus = false;

  const dialogRef = this.dialog.open(ViewEmployeedetailComponent, {               //Pass data object as a second parameter  
    data: {  
      record_id: EMPLOYEEID 
    }  
  });

  dialogRef.afterClosed().subscribe(confirmresult=>{ 
    debugger 
    console.log(confirmresult);  
    if(confirmresult){
      debugger            //if dialog result is yes, delete post  
      //this.GetEmployeeList();
    }  
     
  })  
  
}
  pageEvent(event: PageEvent) {
    this.pageNumber = event.pageIndex + 1;
    this.GetEmployeeList();
  }

}
export interface EmployeeDetails {
  EncyEMPLOYEEID:string
  EMPLOYEEID:string
  EMPLOYEENAME:string,
  EMPLOYEENUMBER:string,
  JOININGDATE:string,
  SOCSEC:string,
  HOURLYPAYRATE:string,
  TOTALAMOUNT:string,
  HOURS:string,
  ACTION:string
}