import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { Router } from '@angular/router';
import { ViewChecksDetailComponent } from '../view-checks-detail/view-checks-detail.component';
import {MatDialog, MatDialogConfig, MatSelect} from "@angular/material";
import {animate, state, style, transition, trigger} from '@angular/animations';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { ReplaySubject, Subject, Observable } from 'rxjs';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil, take, filter, tap, debounceTime, map, delay, finalize } from 'rxjs/operators';
import { LoaderService } from 'src/app/Common/loader.service'

@Component({
  selector: 'app-checks',
  templateUrl: './checks.component.html',
  styleUrls: ['./checks.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]), ],
})
export class ChecksComponent implements OnInit {
  ChecksFrom: FormGroup;
  pageNumber: number = 1;
  pageSize:number = 10;
  totalRecords: number;
  totalPages:number;
  Checkslist=[];
  TaxDeductionList = [];
  expanded: CheckesDetails;
  CheckId :number;
  Isshowtabledata:number=0;
  EmployeecheckData:CheckesDetails[];
  employeedropdownlist=[];
  employeeTodropdownlist=[];
  displayedColumns: string[] = ['StartDate','EndDate','EmployeeName', 'CheckNumber','CheckDate',

  'Gross','TotalDeductionAmount', 'Amount','Action'];
  //CheckesDetailsData:CheckesDetails[];
  TaxDeductionsDetailsData:TaxDeductionsDetails[];

  SubdisplayedColumns: string[] = ['Deductions','Amount'];

  EmployeeCheckSource = new MatTableDataSource(this.EmployeecheckData);
  TaxDeductionsDetailsSource = new MatTableDataSource(this.TaxDeductionsDetailsData);
  
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('openprintmodal', {static: true}) openpopup: ElementRef;
  
   /** control for the MatSelect filter keyword */
  
  Bodyshow: boolean;
  payrollDetail: { PayPeriodId: any; userId: number; createDate: any; payFrequency: any; applicationId: number; createdBy: number; checkDate: any; };
  startEmployeenumber: any;
  lastEmployeenumber: any;
  showempno: boolean=true;

  
/** list of banks filtered by search keyword */
filteredFromEmployeeList: ReplaySubject<FromEmployeeList[]> = new ReplaySubject<FromEmployeeList[]>();
FromEmpDropdownlist: FromEmployeeList[];

filteredToEmployeeList: ReplaySubject<ToEmployeeList[]> = new ReplaySubject<ToEmployeeList[]>();
ToEmpDropdownlist: ToEmployeeList[];
// emp:[];
public EmpFromFilterCtrl: FormControl = new FormControl('',);
public EmpCtrl: FormControl = new FormControl('',);

public EmpToFilterCtrl: FormControl = new FormControl('',);
public EmpToCtrl: FormControl = new FormControl('',);

private _onDestroy = new Subject<void>();
  searching: boolean;

  constructor(private LoaderService:LoaderService,private toastr: ToastrService,private formBuilder: FormBuilder,private router: Router,private empservices:EmployeeService,private dialog: MatDialog) { }

  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1)
    {
      this.ChecksFrom = this.formBuilder.group({
        IsPrintControl: ['1']

      });
      //this.ChecksFrom.controls["IsPrintControl"].setValue(1);
      this.EmployeeCheckSource.sort=this.sort;
      this.GetEmployeeCheckslist();
        
        
      this.EmpFromFilterCtrl.valueChanges
      .pipe(filter(search => !!search),
      tap(() => this.searching = true),
      takeUntil(this._onDestroy),
      map(search=>{debugger
        if(!search) {
        return [];
      }
        return this.getCurrentactivepayperiodEmployee(search).pipe(finalize(() => this.searching = false)); 
      })).subscribe((filteredEmp: Observable<any>) => {debugger
        filteredEmp.subscribe((res : any[]) => { 

          if(!this.EmpCtrl.value && this.EmpToCtrl.value > 0)
            this.EmpCtrl.patchValue(res && res.length > 0 && res[0].id)

          this.filteredFromEmployeeList.next(res.slice()); 
        });
      });

      this.EmpToFilterCtrl.valueChanges
      .pipe(filter(search => !!search),
      tap(() => this.searching = true),
      takeUntil(this._onDestroy),
      map(search=>{debugger
        if(!search) {
        return [];
      }
        return this.getCurrentToactivepayperiodEmployee(search).pipe(finalize(() => this.searching = false)); 
      })).subscribe((filteredEmp: Observable<any>) => {debugger
        filteredEmp.subscribe((res : any[]) => { 
          
          if(!this.EmpToCtrl.value && this.EmpCtrl.value > 0)
            this.EmpToCtrl.patchValue(res && res.length > 0 && res[0].id)
          
          this.filteredToEmployeeList.next(res.slice()); 
        
        });
      });


    }

    else{
      this.router.navigate(['/']);
    }
  }
  
  sortclick(){   
    this.expanded =null;
    this.GetEmployeeCheckslist();
    //this.LoaderService.hide();
  }
 

  openDialogViewCheckDetail(EMPLOYEEID:number,PayperiodId : number)
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.data={
      EmployeeId:EMPLOYEEID,
      PayperiodId : PayperiodId
    }

   
    const dialogRef =this.dialog.open(ViewChecksDetailComponent,dialogConfig);

    // dialogRef.afterClosed().subscribe(confirmresult=>{ 
    //   debugger 
       
    //    this.GetEmployeeCheckslist(this.pageNumber, this.pageSize);
    //   })  
      // return test;
   }
  GetEmployeeCheckslist() {
    debugger
    this.empservices.GetEmployeecheckList(this.pageNumber, this.pageSize).subscribe((res=>{
      if(res){
        debugger;
        let EmployeCheckearray = [];
        // if(res.data.length>0)
        // {
          this.Isshowtabledata=1;
          this.Checkslist=res.data;
          this.Checkslist.forEach(function(value){
            debugger
            let Empcheckdata={
              CheckID : value['checkId'],
              EMPLOYEEID:value['employeeId'],
              PayperiodId:value['payperiodId'],
              EmployeeName:value['firstName']+' '+value['lastName'],
              CheckNumber:value['checkNumber'],
              Gross:value.gross == null ? '0.00':value.gross.toFixed(2), //value['gross'], //.toFixed(2),
              Amount:value['amount'],
              CheckDate: moment(value['checkDate']).format('MM/DD/YYYY') ,
              StartDate: moment(value['startDate']).format('MM/DD/YYYY') ,
              EndDate: moment(value['endDate']).format('MM/DD/YYYY'),
              TotalDeductionAmount : value['totalDeductionAmount'] == null ? '0.00' : value['totalDeductionAmount'].toFixed(2)
              // FederalTaxAmount : value['federalTaxAmount'] === undefined || value['federalTaxAmount'] === null ? '0.00' :value['federalTaxAmount'].toFixed(2), 
              // SocialSecurityTaxAmount	:  value['socialSecurityTaxAmount']  === undefined || value['socialSecurityTaxAmount'] === null ? '0.00' :value['socialSecurityTaxAmount'].toFixed(2), 
              // MedicareTaxAmount : value['medicareTaxAmount'] === undefined || value['medicareTaxAmount'] === null ? '0.00' :value['medicareTaxAmount'].toFixed(2),  
              // StateTaxAmount	: value['stateTaxAmount'] === undefined || value['stateTaxAmount'] === null ? '0.00' :value['stateTaxAmount'].toFixed(2), 
              // AdditionalFederalTaxAmount : value['additionalFederalTaxAmount'] === undefined || value['additionalFederalTaxAmount'] === null ? '0.00' :value['additionalFederalTaxAmount'].toFixed(2),
              // PFML_MedicalAmount	: value['pFML_MedicalAmount'] === undefined || value['pFML_MedicalAmount'] === null ? '0.00' :value['pFML_MedicalAmount'].toFixed(2),
              // PFML_LeaveAmount	: value['pFML_LeaveAmount'] === undefined || value['pFML_LeaveAmount'] === null ? '0.00' :value['pFML_LeaveAmount'].toFixed(2),
              // EmployeeDeductionAmount : value['employeeDeductionAmount'] === undefined || value['employeeDeductionAmount'] === null || value['employeeDeductionAmount'] === 0 ? '0.00' :value['employeeDeductionAmount'].toFixed(2)
  
            }
            EmployeCheckearray.push(Empcheckdata);
          })
          this.EmployeecheckData=EmployeCheckearray;
          this.totalRecords=parseInt(res.meta["totalRecords"]);
          this.totalPages=parseInt(res.meta["totalPages"]);
          this.EmployeeCheckSource.data = this.EmployeecheckData
      //   }
      //  else{
      //    this.Isshowtabledata=2;
      //  }
       
       
      }

    }))
  }
 
  onClick(element: CheckesDetails) {
         
debugger
    // let index = this.expandedElements.findIndex(x => x == element.EMPLOYEEID);
    // if(index > -1)
    //   this.expandedElements.splice(index, 1);
    // else {
    //   this.expandedElements.push(element.EMPLOYEEID)
    // }
      this.expanded = this.expanded === element ? null : element;
      this.CheckId=parseInt(element.CheckID);
      this.TaxDeductionsDetailsSource.data =[];
      // this.GetEmployeeTimepunches(element.EMPLOYEEID);
      this.GetEmployeeTaxDeduction(this.CheckId);
      // this.GetEmployeeList();
    }
    GetEmployeeTaxDeduction(CheckId : number) {
      let _CheckId = CheckId;
      
      this.empservices.GetEmployeeTaxDeduction(_CheckId).subscribe((res=>{
        if(res){
          this.TaxDeductionList=res;
          let EmployeeTimearray = [];
          debugger
          this.TaxDeductionList.forEach(function(value){
            debugger
            let employeedata={
              DeductionTypeName:value["deductionTypeName"],
              Amount:value['amount'] == null ? "0.00" :value['amount'].toFixed(2)
              
            
          }
          EmployeeTimearray.push(employeedata);
          })
          this.TaxDeductionsDetailsData=EmployeeTimearray;
          this.TaxDeductionsDetailsSource.data = this.TaxDeductionsDetailsData
         
         
        }
      }))
      }

  pageEvent(event: PageEvent) {
    this.pageNumber = event.pageIndex + 1;
    this.GetEmployeeCheckslist();
  }

getCurrentactivepayperiodEmployee(Fromemployee): Observable<any>{
  let Employeelist=[];
  let seletedToEmployeeId=this.EmpToCtrl.value || 0;
  return this.empservices.getcurrentFromemployeelist(Fromemployee,seletedToEmployeeId)
  .pipe(
    map((res) => {
      if(res){
        this.employeedropdownlist=res.data;
        // this.employeedropdownlist=res.data.timePunhesViewModel;
        this.employeedropdownlist.forEach(function(value)
        {
          // var Employee = {id:value['employeeId'], Employeename:value['employeeName'] +' ('+value['number']+')'}
          var Employee = {id:value['employeeId'], EmployeeNumber:value['number']}
          Employeelist.push(Employee);
        })
        this.FromEmpDropdownlist=Employeelist;
        return Employeelist;  
      }
    }, 
    () => { return []; 
    })
  );
}
  
getCurrentToactivepayperiodEmployee(Fromemployee): Observable<any>{
  let Employeelist=[];
   let seletedFromEmployeeId=this.EmpCtrl.value || 0;
  return this.empservices.getcurrentToemployeelist(Fromemployee,seletedFromEmployeeId)
  .pipe(
    map((res) => {
      if(res){
        this.employeeTodropdownlist=res.data;
        // this.employeedropdownlist=res.data.timePunhesViewModel;
        this.employeeTodropdownlist.forEach(function(value)
        {
          // var Employee = {id:value['employeeId'], Employeename:value['employeeName'] +' ('+value['number']+')'}
          var Employee = {id:value['employeeId'], EmployeeNumber:value['number']}
          Employeelist.push(Employee);
        })
        this.ToEmpDropdownlist=Employeelist;
        return Employeelist;  
      }
    }, 
    () => { return []; 
    })
  );
}

openModal()  {
debugger
    this.empservices.GetIsChangesOnTimePuches().subscribe((res=>{
      if(res){

        const payrollProcessDetail={
          PayPeriodId:res.data.payPeriodId,
          userId:parseInt(localStorage.getItem("UserID")),
          createDate : res.data.startDate,
          payFrequency : res.data.payFrequency,
          applicationId:0,
          createdBy : parseInt(localStorage.getItem("UserID")),
          checkDate : res.data.checkDate
        }
        this.payrollDetail=payrollProcessDetail;
        this.startEmployeenumber=res.data.startEmployee
        this.lastEmployeenumber=res.data.lastEmployee
        // this.Bodyshow=res.data.processCHK==1?true:this.openpopup.nativeElement.click();
        if(res.data.processCHK==1){
          this.openpopup.nativeElement.dataset.target = "#myModal",
          this.openpopup.nativeElement.click();
          res=[];
        }
        else{
          this.openpopup.nativeElement.dataset.target = "#myPrintCheck",
          this.openpopup.nativeElement.click();
          res=[];
        }

      }
    }))
    
}
  
OnPayrollProcess(){
  debugger

  const payrollProcessDetail={
    PayPeriodId:this.payrollDetail.PayPeriodId,
    userId:parseInt(localStorage.getItem("UserID")),
    createDate :this.payrollDetail.createDate,
    payFrequency : this.payrollDetail.payFrequency,
    applicationId:0,
    createdBy : parseInt(localStorage.getItem("UserID")),
    checkDate :this.payrollDetail.checkDate
  }
  
  this.empservices.Paypayrollprocess(this.payrollDetail).subscribe((res=>{
   
    if (res) {
      debugger
      if(res.statusCode==200){
        debugger
        //  this.GetEmployeePayPeriodlist();
         this.toastr.success(res.message);
         this.openpopup.nativeElement.dataset.target = "#myPrintCheck",
         this.openpopup.nativeElement.click();
        //  this.openpopup
      }else{
        this.toastr.error(res.message);
      }
    
    }
  }))
  
  
}

isprintallcheck(){
  //alert(this.ChecksFrom.controls.IsPrintControl.value);
  debugger
  if(this.showempno)
    this.showempno=false;
  else 
    this.showempno=true;

    //alert(this.showempno);
}

PrintChecks(EmployeeID:number=0,PayPeriodId:number=0,ApplicationId:number=0){
  debugger;
  // this.EmpCtrl.value.EmployeeNumber
  // this.EmpToCtrl.value.EmployeeNumber

  this.empservices.GetIsChangesOnTimePuches().subscribe((res=>{
    if(res){

      const payrollProcessDetail={
        PayPeriodId:res.data.payPeriodId,
        userId:parseInt(localStorage.getItem("UserID")),
        createDate : res.data.startDate,
        payFrequency : res.data.payFrequency,
        applicationId:0,
        createdBy : parseInt(localStorage.getItem("UserID")),
        checkDate : res.data.checkDate
      }
      this.payrollDetail=payrollProcessDetail;
      this.startEmployeenumber=res.data.startEmployee
      this.lastEmployeenumber=res.data.lastEmployee
      // this.Bodyshow=res.data.processCHK==1?true:this.openpopup.nativeElement.click();
      if(res.data.processCHK==1){
        this.openpopup.nativeElement.dataset.target = "#myModal",
        this.openpopup.nativeElement.click();
        res=[];
      }
      else{
      
  let startEmployeenumber:any
  let lastEmployeenumber:any
  
  if(this.showempno) {
    startEmployeenumber = null; lastEmployeenumber = null;
  }
  else{
  if(this.EmpCtrl.valid || this.EmpToCtrl.valid)
  {
    let empFromObj = (this.FromEmpDropdownlist || []).find(x => x.id == this.EmpCtrl.value);
    let empToObj = (this.ToEmpDropdownlist || []).find(x => x.id == this.EmpToCtrl.value);
    startEmployeenumber=empFromObj.EmployeeNumber;
    lastEmployeenumber=empToObj.EmployeeNumber;
  
    if (!empFromObj || !empToObj) {
      return;
  
    }

  }
  else{
    return;
  }
 
  }
  EmployeeID=0,PayPeriodId=3,ApplicationId=3
  this.empservices.PrintEmplyeeChecks(startEmployeenumber,lastEmployeenumber,EmployeeID,PayPeriodId,ApplicationId).subscribe((body=>{
      // get filename from header
      // const contentDisposition = event.headers.get('Content-Disposition');
      // const filename = contentDisposition
      //   .split(';')[1]
      //   .split('filename')[1]
      //   .split('=')[1]
      //   .trim();

      // convert it to file
      const file = new Blob([body], { type: body.type });

      // force to download file
      const blobURL = window.URL.createObjectURL(file);
      const anchor = document.createElement('a');
      anchor.download = 'EmployeesCheck.pdf'; // filename is important to set on backend
      anchor.href = blobURL;
      anchor.click();
      if(this.showempno)
      {
      this.openpopup.nativeElement.dataset.target = "#Continue",
      
      // this.openpopup.nativeElement.dataset.target = "#ClosePaypeiod",
      this.openpopup.nativeElement.click();
      }
  }))
      }

    }
  }))

}

ChangetoEmployeeList(event: any){
  debugger;
  this.EmpToFilterCtrl.patchValue(' ', { emitEvent: true });

  // // this.EmpToFilterCtrl.updateValueAndValidity({ onlySelf: false, emitEvent: true });
  // this.EmpToFilterCtrl.setValue(this.FromEmpDropdownlist[0]);
}
ChangeFromEmployeeList($event){
  debugger;
  this.EmpFromFilterCtrl.patchValue(' ', { emitEvent: true });

  // this.EmpFromFilterCtrl.updateValueAndValidity({ onlySelf: false, emitEvent: true });
  // this.EmpToCtrl.setValue(this.ToEmpDropdownlist[0]);
}
ClosePayPeriod(){


  this.empservices.GetIsChangesOnTimePuches().subscribe((res=>{
    if(res){

      const payrollProcessDetail={
        PayPeriodId:res.data.payPeriodId,
        userId:parseInt(localStorage.getItem("UserID")),
        createDate : res.data.startDate,
        payFrequency : res.data.payFrequency,
        applicationId:0,
        createdBy : parseInt(localStorage.getItem("UserID")),
        checkDate : res.data.checkDate
      }
      this.payrollDetail=payrollProcessDetail;
      this.startEmployeenumber=res.data.startEmployee
      this.lastEmployeenumber=res.data.lastEmployee
      // this.Bodyshow=res.data.processCHK==1?true:this.openpopup.nativeElement.click();
      if(res.data.processCHK==1){
        this.openpopup.nativeElement.dataset.target = "#myModal",
        this.openpopup.nativeElement.click();
        res=[];
      }
      else{
        const payrollProcessDetail={
          PayPeriodId:this.payrollDetail.PayPeriodId,
          userId:parseInt(localStorage.getItem("UserID")),
          createDate :this.payrollDetail.createDate,
          payFrequency : this.payrollDetail.payFrequency,
          applicationId:0,
          createdBy : parseInt(localStorage.getItem("UserID")),
          checkDate :this.payrollDetail.checkDate
        }
        this.empservices.closepayrollprocess(this.payrollDetail).subscribe((res=>{
         
          if (res) {
            debugger
            if(res.statusCode==200){
              debugger
              //  this.GetEmployeePayPeriodlist();
               this.toastr.success(res.message);
               //this.openpopup.nativeElement.dataset.target = "#myPrintCheck",
               this.openpopup.nativeElement.click();
              //  this.openpopup
            }else{
              this.toastr.error(res.message);
            }
          
          }
        }))
      }

    }
  }))


 
}

continuePopup(){
  this.openpopup.nativeElement.dataset.target = "#ClosePaypeiod",
  this.openpopup.nativeElement.click();
}
  

}
export interface CheckesDetails {
  EMPLOYEEID:string
  CheckID:string,
  PayPeriodId:string,
  Gross:string,
  Amount:string,
  CheckNumber:string,
  CheckDate:string,
  EmployeeName:string
}
export interface TaxDeductionsDetails {
  Deductions : string,
  Amount : string
}

 export interface FromEmployeeList {
  id: string;
  EmployeeNumber: string;
}
export interface ToEmployeeList {
  id: string;
  EmployeeNumber: string;
}
