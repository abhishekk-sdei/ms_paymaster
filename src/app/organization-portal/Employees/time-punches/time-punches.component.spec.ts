import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimePunchesComponent } from './time-punches.component';

describe('TimePunchesComponent', () => {
  let component: TimePunchesComponent;
  let fixture: ComponentFixture<TimePunchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimePunchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimePunchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
