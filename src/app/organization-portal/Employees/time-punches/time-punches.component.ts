import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy, } from '@angular/core';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { MatTableDataSource, MatSort, PageEvent, MatSelect } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil, take, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/Common/loader.service'

@Component({
  selector: 'app-time-punches',
  templateUrl: './time-punches.component.html',
  styleUrls: ['./time-punches.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TimePunchesComponent implements OnInit, AfterViewInit, OnDestroy {

  AddTimepuchesForm: FormGroup;
  submitted: boolean;
  expanded: EmployeeDetails;
  status: boolean = false;
  shouldShow: boolean = false;
  pageNumber: number = 1;
  pageSize: number = 10;
  TimePunchCheck: number = 1;
  totalRecords: number;
  totalPages: number;
  employeelist = [];
  Typelist = [];
  employeedropdownlist = [];
  employeeTimepuncheslist = [];
  EmployeeData: EmployeeDetails[];
  EmployeeTimePunchesData: EmployeeTimeDetails[];

  // displayedColumns: string[] = ['Number', 'SocSec', 'FirstName', 'LastName','HourlyRate','Hours','Amount','Status','Action'];
  displayedColumns: string[] = ['Number', 'SocSec', 'FirstName', 'LastName', 'HourlyRate', 'Hours', 'Amount', 'Action'];
  SubdisplayedColumns: string[] = ['Type', 'Date', 'Day', 'IN', 'Out', 'HoursClock', 'Hours', 'HourRate', 'OVT', 'Amount'];
  //SubdisplayedColumns: string[] = ['Type','Date', 'Day', 'IN', 'Out','Hrsclock','Hours','HourRate','OVT','Amount'];
  EmployeeSource = new MatTableDataSource(this.EmployeeData);
  EmployeeTimePunchesSource = new MatTableDataSource(this.EmployeeTimePunchesData);
  // expandedElement: EmployeeDetails;
  /** list of banks filtered by search keyword */
  filteredEmployeeList: ReplaySubject<EmployeeDropDownList[]> = new ReplaySubject<EmployeeDropDownList[]>();
  EmpDropdownlist: EmployeeDropDownList[];
  maxDate: any;
  minDate: any;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
  TotalEmployeeAmount: any;
  TotalEmployeeHours: any;
  TotalEmployeeHoursClock: any;
  EmpId: any;
  EmpHrs: any;


  /** control for the MatSelect filter keyword */
  public EmpFilterCtrl: FormControl = new FormControl();

  private _onDestroy = new Subject<void>();
  employeePayPeriodlist: any;
  currentpayperiodId: number;
  currentstartDate: any;
  currentperiodName: any;
  currentendDate: any;
  selectedempId: any;
  selectedEmpHour: any;
  selectedEmpHPR: any;
  selectedEmpOvt: number;
  selectedEmpAmount: number;
  Isshowtabledata: number = 0;
  noData: any;
  showFrequecny : boolean = false;
  // selectemp: any;
  constructor(private LoaderService:LoaderService, private toastr: ToastrService, private formBuilder: FormBuilder, private EmployeeServices: EmployeeService, private router: Router) {
    // this.expandedElements = [];
  }

  ngOnInit() {
    if (localStorage.getItem("accessToken").length > 1) {

      this.AddTimepuchesForm = this.formBuilder.group({
        //EmployeeNo: ['', [Validators.required]],

        // ReferenceDate: ['', [Validators.required]],
        type: ['', [Validators.required]],
        EmpCtrl: ['', [Validators.required]],
        // EmpCtrl: ['',[Validators.required]],
        Hours: [''],
        HourlyPayRate: [''],
        OvertimeMultiplier: [''],
        // Amount: ['',Validators.compose([Validators.pattern('^[0-9]*(\.\d{2})?%?$')])],
        Amount: [''],
        ReferenceDate: ['', [Validators.required]],

      });

      this.GetEmployeeTimeList();
      this.EmployeeSource.sort = this.sort;
      //setTimeout(() => this.EmployeeSource.sort = this.sort);
      this.EmployeeTimePunchesSource.sort = this.sort;
      this.getemployeedropdown();

      this.EmpFilterCtrl.valueChanges
        .pipe()
        .subscribe(() => {
          this.filterEmployee();
        });
    }
    else {
      this.router.navigate(['/']);
    }
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  get EmployeeTimepunchesDetails() { return this.AddTimepuchesForm.controls; }

  protected setInitialValue() {
    debugger
    this.filteredEmployeeList
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {

        this.singleSelect.compareWith = (a: EmployeeDropDownList, b: EmployeeDropDownList) => a && b && a.id === b.id;
      });
  }

  GetEmployeeTimeList() {
    this.EmployeeServices.GetEmployeeTimeList(this.pageNumber, this.pageSize, this.TimePunchCheck = 1).subscribe((res => {
      if (res) {
       // this.showFrequecny = false;
        if (res.data.timePunhesViewModel.length > 0) {

          this.Isshowtabledata = 1;
          this.employeelist = res.data.timePunhesViewModel;



          let Employeearray = [];
          this.employeelist.forEach(function (value) {
            let employeedata = {

              EMPLOYEEID: value['employeeId'],
              Number: value['number'],
              FirstName: value['firstName'],
              LastName: value['lastName'],
              HourlyRate: parseFloat(value['hourly_PayRate']).toFixed(2),
              Hours: parseFloat(value['totalTime']).toFixed(2),

              HoursClock: value['hoursClock'],
              SocSec: value['socSec'],
              // Amount:value['Amount']
              Amount: parseFloat(value['basicSalary']).toFixed(2),
              Status: value["isActive"] == 1 ? 'Active' : 'In-Active'
            }
            Employeearray.push(employeedata);
          })
          this.EmployeeData = Employeearray;

          this.totalRecords = parseInt(res.meta["totalRecords"]);
          this.totalPages = parseInt(res.meta["totalPages"]);

          // console.log(res);
          this.EmployeeSource.data = this.EmployeeData
          this.noData = this.EmployeeSource.connect().pipe(map(data => data.length === 0));
          // this.EmployeeTimePunchesSource.data = this.EmployeeData
        }
        else {
          this.EmployeeSource.data = [];
          // this.Isshowtabledata=2;
          const data = this.EmployeeSource.data.slice();
          data.shift();
          this.Isshowtabledata = 2;
          this.totalRecords = 0;
          this.totalPages = 0;
          this.noData = this.EmployeeSource.connect().pipe(map(data => data.length === 0));
        }
        this.employeePayPeriodlist = res.data.employeePayPeriodModel;

        this.currentpayperiodId = this.employeePayPeriodlist.payPeriodId;
        this.currentperiodName = this.employeePayPeriodlist.periodName;
        this.currentstartDate = this.employeePayPeriodlist.startDate;
        this.currentendDate = this.employeePayPeriodlist.endDate;
        this.minDate = new Date(this.employeePayPeriodlist.startDate);
        this.maxDate = new Date(this.employeePayPeriodlist.endDate);
        this.showFrequecny = true;
      }
      else {

      } return res;
    }))
  }

  OnTimepunchformSubmit() {

    this.submitted = true;

    if (this.AddTimepuchesForm.valid) {

      const Timepunches = {

        CureentpayPeriodId: this.currentpayperiodId,
        EmployeeId: this.EmployeeTimepunchesDetails.EmpCtrl.value.id,
        ReferenceDate: moment(this.EmployeeTimepunchesDetails.ReferenceDate.value).format("YYYY-MM-DD"),
        type: this.EmployeeTimepunchesDetails.type.value.toString(),
        HourlyPayRate: parseFloat(this.EmployeeTimepunchesDetails.HourlyPayRate.value),
        Hours: parseFloat(this.EmployeeTimepunchesDetails.Hours.value),
        Amount: parseFloat(this.EmployeeTimepunchesDetails.Amount.value),
        OvertimeMultiplier: parseFloat(this.EmployeeTimepunchesDetails.OvertimeMultiplier.value),
        UserId: parseInt(localStorage.getItem("UserID")),
        CreatedBy: parseInt(localStorage.getItem("UserID"))
      }

      this.EmployeeServices.AddTimePunches(Timepunches).subscribe((res => {
        if (res) {
          debugger
          this.toastr.success(res.message);
          // localStorage.setItem("accessToken", res.value.token);
          // this.router.navigate(['/home/TimePunches']);
          this.status = !this.status;
          this.GetEmployeeTimepunches(Timepunches.EmployeeId);


          this.AddTimepuchesForm.reset();
        }
      }))
    }
  }


  pageEvent(event: PageEvent) {
    this.pageNumber = event.pageIndex + 1;
    this.GetEmployeeTimeList();
  }

  test(event: any) {

    console.log(event)
  }

  onClick(element: EmployeeDetails) {


    // let index = this.expandedElements.findIndex(x => x == element.EMPLOYEEID);
    // if(index > -1)
    //   this.expandedElements.splice(index, 1);
    // else {
    //   this.expandedElements.push(element.EMPLOYEEID)
    // }
    this.expanded = this.expanded === element ? null : element;
    this.EmpId = parseInt(element.EMPLOYEEID);
    this.EmployeeTimePunchesSource.data = [];
    // this.GetEmployeeTimepunches(element.EMPLOYEEID);
    this.GetEmployeeTimepunches(this.EmpId);
    // this.GetEmployeeList();
  }

  getHoursSum() {
    let arr = [...this.EmployeeTimePunchesSource.data];
    this.TotalEmployeeHours = parseFloat(arr.map(x => parseFloat(x.Hours || '0')).reduce((acc, Hours) => acc + Hours, 0).toFixed(2)).toFixed(2);
    // return arr.map(x=>parseFloat(x.Hours)).reduce((acc,Hours)=>acc+Hours,0);
    this.EmployeeSource.data.filter(x => x.EMPLOYEEID == this.EmpId)[0].Hours = this.TotalEmployeeHours;
    return this.TotalEmployeeHours;

  }
  getHoursClock() {
    let arr = [...this.EmployeeTimePunchesSource.data];
    this.TotalEmployeeHoursClock = parseFloat(arr.map(x => parseFloat(x.Hrsclock)).reduce((acc, Hrsclock) => acc + Hrsclock, 0).toFixed(2));
    // return   (arr.map(x=>parseFloat(x.Hrsclock)).reduce((acc,Hrsclock)=>acc+Hrsclock,0));
    return this.TotalEmployeeHoursClock;

  }
  click(){
    this.expanded =null;
    this.GetEmployeeTimeList();
    //this.LoaderService.hide();
  }
  getHoursDiff(element: EmployeeTimeDetails) {

    let IN = moment(element.IN, "HH:mm:ss ");
    let Out = moment(element.Out, "HH:mm:ss a");
    let duration = moment.duration(Out.diff(IN));
    element.Hrsclock = duration.asHours().toFixed(2);
    return duration.asHours().toFixed(2);
  }
  OnblurMethod(element: EmployeeTimeDetails) {


    if (element.Type == "B") {
      this.getTotalAmount();
      this.UpdateTimePunches(element);
    }
    else {
      this.getHoursSum();
      this.getHoursClock();
      element.Hours = element.Hours || '0';
      element.HourlyRate = element.HourlyRate || '0'
      element.OVT = element.OVT || '0';
      this.getAmount(element);
      this.getTotalAmount();
      this.UpdateTimePunches(element);
    }



  }

  async  UpdateTimePunches(element: EmployeeTimeDetails) {

    let arr = [...this.EmployeeTimePunchesSource.data];
    let Timepunches = [];
    let id = this.EmpId;
    let EmpHrs = this.EmpHrs
    let TotalEmployeeAmount = parseFloat(this.TotalEmployeeAmount);
    let TotalEmployeeHoursClock = parseFloat(this.TotalEmployeeHoursClock);
    let TotalEmployeeHours = parseFloat(this.TotalEmployeeHours);
    if (element.Out.toLowerCase().includes("am") || element.Out.toLowerCase().includes("pm")) {
      element.Out = moment(element.Out, "h:mm:ss A").format("HH:mm")
    }
    else {
      element.Out = element.Out;
    }
    const timepunches = {
      CureentpayPeriodId: this.currentpayperiodId,
      TimePunchId: element.TimePunchId,
      EmployeeId: id,
      // InDateTime:moment(element.IN).format("hh:mm"),
      InDateTime: element.IN,

      outDateTime: element.Out,
      // OutDateTime:moment(element.Out,"hh:mm:ss a").format("hh:mm"),
      checkId: element.checkId,
      ReferenceDate: moment(element.Date).format("YYYY-MM-DD"),
      Type: element.Type,
      Hours: parseFloat(element.Hours),
      HoursClock: element.Hrsclock,
      HourlyPayRate: parseFloat(element.HourlyRate),
      OvertimeMultiplier: parseFloat(element.OVT),
      Amount: parseFloat(element.Amount),
      TotalAmount: TotalEmployeeAmount,
      TotalHrsclock: TotalEmployeeHoursClock,
      TotalHrs: TotalEmployeeHours,
      EmpHourlyPayRate: EmpHrs,
      UpdatedBy: parseInt(localStorage.getItem("UserID"))
    }

    var asyncResult = await this.EmployeeServices.updateTimepunches(timepunches).subscribe((res => {
      console.log(res);


    }))
    // console.log(arr);
  }
  noRecord=false;
  GetEmployeeTimepunches(Emolyeeid: number) {
    debugger
    this.noRecord=false;
    let tempEmpID = Emolyeeid;
    this.EmployeeServices.GetEmployeeTimepunches(Emolyeeid).subscribe((res => {
      debugger;
      if (res.length!="0") {
        this.noRecord=false;
        this.employeeTimepuncheslist = res;
        let EmployeeTimearray = [];
        this.employeeTimepuncheslist.forEach(function (value) {
          debugger
          let employeedata = {
            checkId: value["checkId"],
            TimePunchId: value["timePunchId"],
            Type: value['type'].trim(),
            // IN:value['inDateTime'],
            // Out:value['outDateTime'],
            IN: value['inDateTime'] ? moment(value['inDateTime']).format("hh:mm") : "12:00",
            Out: value['outDateTime'] ? moment(value['outDateTime']).format("hh:mm a") : "12:00",
            Date: value['createdDate'],
            HourlyRate: value['hourlyPayRate'] == null ? 0 : value['hourlyPayRate'],
            Hours: value['hours'] == null ? 0 : value['hours'],
            HoursClock: value["hoursClock"] == null ? 0 : value["hoursClock"],
            OVT: value['overtimeMultiplier'] == null ? 0 : value['overtimeMultiplier'],
            Amount: value['amount'] == null ? "0.00" : value['amount'].toFixed(2)


          }
          EmployeeTimearray.push(employeedata);
        })
        this.EmployeeTimePunchesData = EmployeeTimearray;
        this.EmployeeTimePunchesSource.data = this.EmployeeTimePunchesData;
        this.TotalEmployeeAmount = this.EmployeeTimePunchesData.map(x => parseFloat(x.Amount)).reduce((acc, amount) => acc + amount, 0).toFixed(2);
        this.EmployeeSource.data.filter(x => x.EMPLOYEEID == tempEmpID.toString())[0].Amount = this.TotalEmployeeAmount;
      }
      else if(res.length=="0"){
        this.noRecord=true;
      }

    }))
  }
  getAmount(element: EmployeeTimeDetails) {

    let Hrsclock = parseFloat(parseFloat(element.Hrsclock).toFixed(2));
    let Hours = parseFloat(parseFloat(element.Hours || '0').toFixed(2));
    let HourlyRate = parseFloat(parseFloat(element.HourlyRate || '0').toFixed(2));
    let OVT = parseFloat(parseFloat(element.OVT || '0').toFixed(2));
    let amount;

    amount = HourlyRate * Hours * OVT;


    element.Amount = amount.toFixed(2);
    return amount.toFixed(2);
  }
  getTotalAmount() {
    let arr = [...this.EmployeeTimePunchesSource.data];
    this.TotalEmployeeAmount = arr.map(x => parseFloat(x.Amount)).reduce((acc, amount) => acc + amount, 0).toFixed(2);
    this.EmployeeSource.data.filter(x => x.EMPLOYEEID == this.EmpId)[0].Amount = this.TotalEmployeeAmount;
    // this.GetEmployeeList();
    return arr.map(x => parseFloat(x.Amount)).reduce((acc, amount) => acc + amount, 0).toFixed(2);
  }

  getSelectedEmployeeDetails(EmpCtrl: any) {
    debugger;
    let tempempId: number;
    if (EmpCtrl.value) {
      tempempId = EmpCtrl.value.id;
    }
    else {
      tempempId = EmpCtrl;
    }

    this.EmployeeServices.GetEmployeeById(tempempId).subscribe((res => {
      this.selectedEmpHour = res.data.hours;
      this.selectedEmpHPR = res.data.hourly_PayRate;
      this.selectedEmpOvt = 1;
      this.selectedEmpAmount = parseFloat(res.data.hours) * parseFloat(res.data.hourly_PayRate) * 1;
      this.AddTimepuchesForm.get("Hours").patchValue(res.data.hours)
      this.AddTimepuchesForm.get("HourlyPayRate").patchValue(res.data.hourly_PayRate)
      this.AddTimepuchesForm.get("OvertimeMultiplier").patchValue(1)
      if (this.AddTimepuchesForm.controls.type.value == 4)
        this.AddTimepuchesForm.get("Amount").patchValue('')
      else
        this.AddTimepuchesForm.get("Amount").patchValue(parseFloat(res.data.hours) * parseFloat(res.data.hourly_PayRate) * 1)
    }))

  }

  getAmountOnTimepunche() {
    debugger
    let amount = parseFloat(this.EmployeeTimepunchesDetails.HourlyPayRate.value) * parseFloat(this.EmployeeTimepunchesDetails.Hours.value) * parseFloat(this.EmployeeTimepunchesDetails.OvertimeMultiplier.value);
    this.AddTimepuchesForm.get("Amount").patchValue(amount.toFixed(2));
  }

  isActive(element?: EmployeeDetails) {
    debugger

    this.selectedempId = element && element.EMPLOYEEID ? element.EMPLOYEEID : 0;
    let selectedEmployeeIndex = this.EmpDropdownlist.findIndex(x => x.id === this.selectedempId);

    this.AddTimepuchesForm.get('EmpCtrl').setValue(this.EmpDropdownlist[selectedEmployeeIndex]);



    this.EmployeeTimepunchesDetails
    if (this.selectedempId != 0)
      this.getSelectedEmployeeDetails(this.selectedempId);
    if (this.selectedempId > 0)
      this.AddTimepuchesForm.get("EmpCtrl").disable();
    else
      this.AddTimepuchesForm.get("EmpCtrl").enable();

    this.status = !this.status;
  }
  isdActive() {
    this.status = !this.status;
    this.AddTimepuchesForm.reset();
    this.ngOnDestroy();
  }

  ToggleTimeShow(index) {
    this.shouldShow = !this.shouldShow;
  }

  getemployeedropdown() {
    debugger
    var Employeelist = [];


    this.EmployeeServices.GetEmployeeTimeList(this.pageNumber, this.pageSize, this.TimePunchCheck = 0).subscribe(res => {
      if (res) {
debugger;
        // this.employeedropdownlist=res.data;
        this.employeedropdownlist = res.data.timePunhesViewModel;
        this.employeedropdownlist.forEach(function (value) {
          var Employee = { id: value['employeeId'], Employeename: value['employeeName'] + ' (' + value['number'] + ')' }
          Employeelist.push(Employee);
        })
        this.EmpDropdownlist = Employeelist;



        this.filteredEmployeeList.next(this.EmpDropdownlist.slice());


      }
    })


    this.EmployeeServices.GetTypelist().subscribe((res => {
      if (res) {
        this.Typelist = res || [];
        // console.log(res);
      }
    }));
  }

  private filterEmployee() {


    if (!this.EmpDropdownlist) {
      return;
    }
    // get the search keyword
    let search = this.EmpFilterCtrl.value;
    if (!search) {
      this.filteredEmployeeList.next(this.EmpDropdownlist.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredEmployeeList.next(
      this.EmpDropdownlist.filter(Empname => Empname.Employeename.toLowerCase().indexOf(search) > -1)
    );
  }

  show = true;

  disabled(typeName) {
    if (typeName == "Bonus") {
      this.show = false;
      this.AddTimepuchesForm.get("Hours").patchValue('')
      this.AddTimepuchesForm.get("HourlyPayRate").patchValue('')
      this.AddTimepuchesForm.get("OvertimeMultiplier").patchValue('')
      this.AddTimepuchesForm.get("Amount").patchValue('')
      this.AddTimepuchesForm.get("Amount").enable();

    }
    else {
      this.show = true;



      this.AddTimepuchesForm.get("Hours").patchValue(this.selectedEmpHour)
      this.AddTimepuchesForm.get("HourlyPayRate").patchValue(this.selectedEmpHPR)
      this.AddTimepuchesForm.get("OvertimeMultiplier").patchValue(this.selectedEmpOvt)
      this.AddTimepuchesForm.get("Amount").patchValue(this.selectedEmpAmount)
      this.AddTimepuchesForm.get("Amount").disable();

    }
  }
}


export interface EmployeeDetails {
  EMPLOYEEID: string
  FirstName: string,
  LastName: string,
  Number: string,
  HourlyRate: string,
  Hours: string,
  Amount: string,
  SocSec: string,
  Status: string,
  ACTION: string
}
export interface EmployeeTimeDetails {
  TimePunchId: string,
  checkId: string,
  Type: string
  IN: string,
  Out: string,
  Date: string,
  HourlyRate: string,
  Hours: string,
  Hrsclock: string,
  Amount: string,
  OVT: string,
}
export interface EmployeeDropDownList {
  id: string;
  Employeename: string;
}





