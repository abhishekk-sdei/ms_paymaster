import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEmployeedetailComponent } from './view-employeedetail.component';

describe('ViewEmployeedetailComponent', () => {
  let component: ViewEmployeedetailComponent;
  let fixture: ComponentFixture<ViewEmployeedetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEmployeedetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEmployeedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
