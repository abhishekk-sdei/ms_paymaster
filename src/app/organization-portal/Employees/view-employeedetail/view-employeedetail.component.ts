import { Component, OnInit,Inject  } from '@angular/core';
import {FormBuilder,FormGroup} from '@angular/forms';
import {EmployeeService} from 'src/app/Services/EmployeeServices';
import {Router,ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA} from '@angular/material'; 

@Component({
  selector: 'app-view-employeedetail',
  templateUrl: './view-employeedetail.component.html',
  styleUrls: ['./view-employeedetail.component.scss']
})
export class ViewEmployeedetailComponent implements OnInit {

  EmployeeNumber : any;
  SocSec : any;
  EmployeeName:any;
  HourlyPayRate : any;
  DailyRate : any;
  MaritalStatus : any;
  BirthDate : any;
  RaceName : any;
  Gender : any;
  CityId : any;
  StateName : any;
  CountryName : any;
  JoiningDate : any;
  Address : any;
  BasicSalary : any;
  GrossSalary : any;
  EarnedIncomeCredit : any;
  EarnedIncomeCreditChildren : any;
  PayorBankNumber : any;
  EmpId : any;
  Check : boolean = false;
  HeadOfHousehold: any;
  Hours : any;
  PeriodName : any;
Blind: any;
Student: any;
FlatFit : any;
AdditionalWitholdingAmount : any;
Allowances : any;
Dependents : any;
Exemptions : any;
  constructor(private employeeService:EmployeeService,private router: Router,private route:ActivatedRoute,@Inject(MAT_DIALOG_DATA) public data:any) { }

  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1){
debugger;
    this.route.queryParams.subscribe(par =>
      {
        debugger;
        this.EmpId = this.data.record_id;
        this.Check = false;

       //if(par['EmpId']>0)
       if(this.EmpId > 0)
       {
        //this.EmpId = parseInt(par['EmpId']);
        this.employeeService.GetEmployeeById(this.EmpId).subscribe((res=>
          {
            debugger;
           console.log(res); 
           this.EmpId = res.data['employeeID'];
           this.EmployeeName = 
           ((res.data['employeeName'] == undefined || res.data['employeeName'] == null)
           ?
           res.data['firstName'] +
           ((res.data['middleName'] == undefined || res.data['middleName'] == null)?'':' '+res.data['middleName'])
           +((res.data['lastName'] == undefined || res.data['lastName'] == null)?'':' '+res.data['lastName'])
           :
           res.data['employeeName'] );
           //)
           this.EmployeeNumber = res.data['number'];
           this.SocSec = res.data['socSec'];
           this.Gender = res.data['genderName'];
           this.RaceName = res.data['raceName'];
           this.MaritalStatus = res.data['maritalStatusName'];
           this.HourlyPayRate = res.data['hourly_PayRate'];
           this.DailyRate = res.data['dailyRate'];
          // this.Address = res.data['CityName'] +', '+res.data['StateName']+', '+res.data['CountryName'];
          this.BirthDate = res.data['birth_Date']; 
          this.CountryName = res.data['countryName'];
           this.StateName = res.data['stateName'];
           this.CityId = res.data['cityName'];
           this.JoiningDate = res.data['joiningDate'];
           debugger
           this.BasicSalary = res.data['basicSalary'];
           this.GrossSalary = res.data['grossSalary'];

           this.EarnedIncomeCredit = res.data['earnedIncomeCredit'];
           this.EarnedIncomeCreditChildren = res.data['earnedIncomeCreditChildren'];
           this.PayorBankNumber = res.data['payorBankNumber'];
           this.HeadOfHousehold = res.data['headOfHousehold'] == true ? 'Yes':'No';
           this.Blind = res.data['blind'] == 'S' ? 'Single':res.data['blind'] == 'B' ?'Both':'No';
           this.Student = res.data['student'] == true ? 'Yes':res.data['student'] == false ?'No':'NA';
           this.Hours = res.data['hours'].toFixed(2),
           this.PeriodName = res.data['periodName'],
           this.FlatFit = res.data['flatFit'].toFixed(2),
           this.AdditionalWitholdingAmount = res.data['additionalWitholdingAmount'].toFixed(2),          
           this.Allowances = res.data["allowances"] == null ? 'NA' : res.data["allowances"] ,
           this.Dependents = res.data["dependents"] == null ? 'NA' : res.data["dependents"] ,
           this.Exemptions = res.data["exemptions"] == null ? 'NA' : res.data["exemptions"] ,
           this.Check = true;
          }))
       } 
      })
    }
    else{
      this.router.navigate(['/']);
    }
  }

}
