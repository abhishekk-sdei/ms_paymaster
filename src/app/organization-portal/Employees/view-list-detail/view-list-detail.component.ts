import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  earning: string;
  rate: string;
  hours: string;
  amount: string;
  deduction: string;
  tamount:string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {earning: 'Regular pay', rate: '$80.00', hours: '160.00', amount: '$12,800.00',deduction: 'Income Tax',tamount: '$5000.00',},
  
  {earning: 'Regular pay', rate: '$80.00', hours: '160.00', amount: '$12,800.00',deduction: 'Income Tax',tamount: '$5000.00',},
  {earning: 'Regular pay', rate: '$80.00', hours: '160.00', amount: '$12,800.00',deduction: 'Income Tax',tamount: '$5000.00',},
  {earning: 'Regular pay', rate: '$80.00', hours: '160.00', amount: '$12,800.00',deduction: 'Income Tax',tamount: '$5000.00',},
  
];

@Component({
  selector: 'app-view-list-detail',
  templateUrl: './view-list-detail.component.html',
  styleUrls: ['./view-list-detail.component.scss']
})
export class ViewListDetailComponent implements OnInit {
  displayedColumns: string[] = ['earning', 'rate', 'hours', 'amount', 'deduction', 'tamount'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit() {
  }

}
