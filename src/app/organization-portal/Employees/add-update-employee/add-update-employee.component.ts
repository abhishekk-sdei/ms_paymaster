import { Component, OnInit,Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { Router } from '@angular/router';
import { MatSelect } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import {  EmployeeValidators } from '../Validations/user.validator';
//import {OnlyNumberServices} from 'src/app/Services/OnlyNumberServices';
@Component({
  selector: 'app-add-update-employee',
  templateUrl: './add-update-employee.component.html',
  styleUrls: ['./add-update-employee.component.scss']
})
export class AddUpdateEmployeeComponent implements OnInit {

  
  //EmployeeFrom: FormGroup;
  EmployeeBasicFrom : FormGroup;
  EmployeeSalaryFrom : FormGroup;
  EmployeePersonalFrom : FormGroup;
  EmployeePaymentFrom : FormGroup;
  submitted: boolean;
  CountryList: any;
  GenderList: any;
  PayFrequencyList: any;
  RaceList: any;
  MaritalList: any;
  CityList: any;
  Statelist: any;
  cid: any;
  TempEmpId :number;
  maxDate=new Date();
  constructor(private Empvalidate:EmployeeValidators,private toastr: ToastrService,private formBuilder: FormBuilder,private emplyeeService:EmployeeService,private router: Router) { }

  ngOnInit() {
    localStorage.setItem("CurrentEmpid","");
    if(localStorage.getItem("accessToken").length>1)
    {
    // this.EmployeeFrom = this.formBuilder.group({
    //   //EmployeeNo: ['', [Validators.required]],
    //   Socsec: ['', Validators.compose([Validators.required]),this.Empvalidate.SocSecExist()],
    //   FirstName: ['', [Validators.required]],
    //   MiddleName: ['',],
    //   LastName: ['', [Validators.required]],
    //   Gender: ['', [Validators.required]],
    //   Race: ['', [Validators.required]],
    //   Maritalstatus: ['', [Validators.required]],
    //   Birthday: ['', [Validators.required]],
    //   Country: ['', [Validators.required]],
    //   State: ['', [Validators.required]],
    //   City: ['', [Validators.required]],
    //   HourlyPayRate: ['', [Validators.required]],
    //   Hours: ['', [Validators.required]],
    //   DateofJoining: ['', [Validators.required]],

    // });
    this.EmployeePersonalFrom = this.formBuilder.group({
      //EmployeeNo: ['', [Validators.required]],
  
      Country: ['', [Validators.required]],
      State: ['', [Validators.required]],
      City: ['', [Validators.required]],
      HeadOfHousehold: ['',],
      Blind: ['', ],
      Student: ['',],

    });
    this.EmployeeSalaryFrom = this.formBuilder.group({
      //EmployeeNo: ['', [Validators.required]],
  
     
      HourlyPayRate: ['', [Validators.required]],
      Hours: ['', [Validators.required]],
      PayPeriod: ['', [Validators.required]],

      IncomeCredit: ['', ],
      IncomeCreditChildren: ['', ],
      Allowances: ['',],

      Dependents: ['', ],
      Exemptions: ['',],
      FlatFit: ['', ],
      AdditionalWitholdingAmount: ['',],
      PartTime: ['',],

      

    });
    this.EmployeeBasicFrom = this.formBuilder.group({
      //EmployeeNo: ['', [Validators.required]],
      Socsec: ['', Validators.compose([Validators.required]),this.Empvalidate.SocSecExist()],
      FirstName: ['', [Validators.required]],
      MiddleName: ['',],
      LastName: ['', [Validators.required]],
      Gender: ['', [Validators.required]],
      Race: ['', [Validators.required]],
      Maritalstatus: ['', [Validators.required]],
      Birthday: ['', [Validators.required]],
      DateofJoining: ['', [Validators.required]],

    });

    this.emplyeeService.GetGenderList().subscribe((res=>{
      if(res){
        this.GenderList=res||[];
        // console.log(res);
      }else{

      }
    }));


    
    this.emplyeeService.GetPayFrequencyList().subscribe((res=>{
      if(res){
        this.PayFrequencyList=res||[];
        // console.log(res);
      }else{

      }
    }));

    this.emplyeeService.GetRaceList().subscribe((res=>{
      if(res){
        this.RaceList=res||[];
        // console.log(res);
      }else{

      }
    }));

    this.emplyeeService.GetMaritalList().subscribe((res=>{
      if(res){
        this.MaritalList=res||[];
      }else{

      }
    }));

    this.emplyeeService.GetCountryList().subscribe((res=>{
      if(res){
        this.CountryList=res||[];
        // console.log(res);
      }else{

      }
    }));
  }
  else{
    this.router.navigate(['/']);
  }
    // this.emplyeeService.GetCityList()

  }
  //get EmployeeDetails() { return this.EmployeeFrom.controls; }

  get EmployeeBasicDetails() { return this.EmployeeBasicFrom.controls; }
  get EmployeeSalaryDetails() { return this.EmployeeSalaryFrom.controls; }
  get EmployeePersonalDetails() { return this.EmployeePersonalFrom.controls; }

  OnCountrySelect(event:MatSelect){
    debugger;
    let CountryId = event.value;
    this.cid=CountryId
    this.emplyeeService.GetStateList(CountryId).subscribe((res=>{
      if(res){
        this.Statelist=res||[];
        console.log(res);
      }else{

      }
    }));
  }


  OnStateSelect(event:MatSelect){
    debugger;
    let StateID = event.value;
    this.emplyeeService.GetCityList(this.cid,StateID).subscribe((res=>{
      if(res){
        this.CityList=res||[];
        // console.log(res);
      }else{

      }
    }));
  }

  OnEmpformSubmit(i:number){
    
    this.submitted = true;
    debugger;
    //alert(i);
    if(i == 1)
    {
      if(this.EmployeeBasicFrom.valid){
        debugger;
       
        const EmpBasicdata={
          
        //Number: this.EmployeeDetails.EmployeeNo.value,
        Socsec:this.EmployeeBasicDetails.Socsec.value ,
        FirstName: this.EmployeeBasicDetails.FirstName.value,
        MiddleName: this.EmployeeBasicDetails.MiddleName.value,
        LastName:this.EmployeeBasicDetails.LastName.value,
        Sex: this.EmployeeBasicDetails.Gender.value,
        Race: this.EmployeeBasicDetails.Race.value,
        Maritalstatus: this.EmployeeBasicDetails.Maritalstatus.value,
        BirthDate: moment(this.EmployeeBasicDetails.Birthday.value).format("YYYY-MM-DD"),
        Hiredate : moment(this.EmployeeBasicDetails.DateofJoining.value).format("YYYY-MM-DD"),
        UserId:parseInt(localStorage.getItem("UserID")),
        CreatedBy:parseInt(localStorage.getItem("UserID"))
        }
        debugger;
        this.emplyeeService.AddEmployeeBasicDetails(EmpBasicdata).subscribe((res=>{
          if (res) {
            debugger
            this.TempEmpId = res.data;
            this.toastr.success(res.message);
            // localStorage.setItem("accessToken", res.value.token);
              //this.router.navigate(['/home/employee']);
           } 
        }))
      }
    }
    else if(i == 2)
    {
      if(this.EmployeeSalaryFrom.valid){
        debugger;
       
        const EmpSalarydata={
          
          EmployeeID:this.TempEmpId,
          PayFrequency : this.EmployeeSalaryDetails.PayPeriod.value,
        HourlyPayRate: parseFloat(this.EmployeeSalaryDetails.HourlyPayRate.value),


        EarnedIncomeCredit  : parseInt(this.EmployeeSalaryDetails.IncomeCredit.value),
        EarnedIncomeCreditChildren:parseInt(this.EmployeeSalaryDetails.IncomeCreditChildren.value),
        Allowances: parseInt(this.EmployeeSalaryDetails.Allowances.value),
  
        Dependents: parseInt(this.EmployeeSalaryDetails.Dependents.value),
        Exemptions: parseInt(this.EmployeeSalaryDetails.Exemptions.value),
        FlatFit: parseFloat(this.EmployeeSalaryDetails.FlatFit.value),
        AdditionalWitholdingAmount : parseFloat(this.EmployeeSalaryDetails.AdditionalWitholdingAmount.value),
        PartTime : 1,

        Hours: parseFloat(this.EmployeeSalaryDetails.Hours.value),
        //UserId:parseInt(localStorage.getItem("UserID")),
        //CreatedBy:parseInt(localStorage.getItem("UserID"))
        }
        debugger;
        this.emplyeeService.AddEmployeeSalaryDetails(EmpSalarydata).subscribe((res=>{
          if (res) {
            debugger
            this.toastr.success(res.message);
            // localStorage.setItem("accessToken", res.value.token);
              //this.router.navigate(['/home/employee']);
           } 
        }))
      }
    }
    else if(i == 3)
    {
      if(this.EmployeePersonalFrom.valid){
        debugger;
       
        const EmpPersonaldata={
          EmployeeID:this.TempEmpId,
          CountryId:this.EmployeePersonalDetails.Country.value,
          StateId:this.EmployeePersonalDetails.State.value,
          CityId: this.EmployeePersonalDetails.City.value,
          HeadOfHousehold : parseInt(this.EmployeePersonalDetails.HeadOfHousehold.value) == 1,
          Blind : this.EmployeePersonalDetails.Blind.value,
          Student :  parseInt(this.EmployeePersonalDetails.Student.value) == 1,
        //UserId:parseInt(localStorage.getItem("UserID")),
        //CreatedBy:parseInt(localStorage.getItem("UserID"))
        }
        debugger;
        this.emplyeeService.AddEmployeePersonalDetails(EmpPersonaldata).subscribe((res=>{
          if (res) {
            debugger
            this.toastr.success(res.message);
            // localStorage.setItem("accessToken", res.value.token);
              //this.router.navigate(['/home/employee']);
           } 
        }))
      }
    } 
    else if(i == 4)
    {
      
    }
    
  }

  // OnEmpformSubmit(){
    
  //   this.submitted = true;
  //   debugger;
  //   //alert(i);
    
  //   if(this.EmployeeFrom.valid){
  //     debugger;
     
  //     const Empdata={
        
  //     //Number: this.EmployeeDetails.EmployeeNo.value,
  //     Socsec:this.EmployeeDetails.Socsec.value ,
  //     FirstName: this.EmployeeDetails.FirstName.value,
  //     MiddleName: this.EmployeeDetails.MiddleName.value,
  //     LastName:this.EmployeeDetails.LastName.value,
  //     Sex: this.EmployeeDetails.Gender.value,
  //     Race: this.EmployeeDetails.Race.value,
  //     Maritalstatus: this.EmployeeDetails.Maritalstatus.value,
  //     BirthDate: moment(this.EmployeeDetails.Birthday.value).format("YYYY-MM-DD"),
  //     CountryId:this.EmployeeDetails.Country.value,
  //     StateId:this.EmployeeDetails.State.value,
  //     CityId: this.EmployeeDetails.City.value,
  //     HourlyPayRate: parseFloat(this.EmployeeDetails.HourlyPayRate.value),
  //     Hours: parseFloat(this.EmployeeDetails.Hours.value),
  //     UserId:parseInt(localStorage.getItem("UserID")),
  //     CreatedBy:parseInt(localStorage.getItem("UserID"))
  //     }
  //     debugger;
  //     this.emplyeeService.AddEmployee(Empdata).subscribe((res=>{
  //       if (res) {
  //         debugger
  //         this.toastr.success(res.message);
  //         // localStorage.setItem("accessToken", res.value.token);
  //           this.router.navigate(['/home/employee']);
  //        } 
  //     }))
  //   }
  // }

 
}
export class NumericDirective {

  
}
