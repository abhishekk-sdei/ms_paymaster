import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChecksDetailComponent } from './view-checks-detail.component';

describe('ViewChecksDetailComponent', () => {
  let component: ViewChecksDetailComponent;
  let fixture: ComponentFixture<ViewChecksDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChecksDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChecksDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
