import { Component, OnInit,Inject  } from '@angular/core';
import {FormBuilder,FormGroup} from '@angular/forms';
import {EmployeeService} from 'src/app/Services/EmployeeServices';
import {Router,ActivatedRoute} from '@angular/router';
import {MAT_DIALOG_DATA} from '@angular/material'; 

@Component({
  selector: 'app-view-checks-detail',
  templateUrl: './view-checks-detail.component.html',
  styleUrls: ['./view-checks-detail.component.scss']
})
export class ViewChecksDetailComponent implements OnInit {

  IncomeTax : any;
  FederalTax : any;
  Medical : any;
  LifeInsurance : any;
  ProvidentFund : any;    
 FederalTaxAmount : any;
SocialSecurityTaxAmount	: any;
MedicareTaxAmount	: any;
StateTaxAmount	: any;
AdditionalFederalTaxAmount : any;
PFML_MedicalAmount	: any;
PFML_LeaveAmount	: any;
EmployeeDeductionAmount : any;


  EmployeeNumber : any;
  Regular : any;
  EmployeeName:any;
  HourlyPayRate : any;
  DailyRate : any;
  Bonus : any;
  Vacation : any;
  Holiday : any;  
  BasicSalary : any;
  GrossSalary : any;  
  PayorBankNumber : any;
  EmpId : number;
  PayPeriodId : number;
  ApplicationId : number;
  Check : boolean = false;
  CheckNumber : any;
  CheckDate : any;
  StartDate : any;
  EndDate : any;
  PaidDate : any;
  OrganizationName : any;
  OrganizationUrl : any;
  OrganizationEmail : any;

  Allowances : any;
Dependents : any;
Exemptions : any;
TotalEarning : any;
TotalDeduction : any;
TotalPay : any;
RegularHour : any;
HolidayHour : any;
VacationHour : any;
BonusHour : any;
RegularPayRate: any;
HolidayPayRate: any;
VacationPayRate : any;
BonusPayRate : any;
RegularAssignedHours  : any;
HolidayAssignedHours : any; 
VacationAssignedHours : any;
BonusAssignedHours : any; 


  constructor(private employeeService:EmployeeService,private router: Router,private route:ActivatedRoute,@Inject(MAT_DIALOG_DATA) public data:any) { }

  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1){
      debugger;
          this.route.queryParams.subscribe(par =>
            {
              debugger;
              this.EmpId = this.data.EmployeeId;
              this.PayPeriodId = this.data.PayperiodId;
              this.ApplicationId = 0;
              this.Check = false;
      
             //if(par['EmpId']>0)
             if(this.EmpId > 0)
             {
              //this.EmpId = parseInt(par['EmpId']);
              this.employeeService.ViewChecksDetail(this.EmpId,this.PayPeriodId,this.ApplicationId).subscribe((res=>
                {
                  debugger;
                 console.log(res); 
                 this.EmployeeName = 
                 res['firstname'] +
                 ((res['middleName'] == undefined || res['middleName'] == null)?'':' '+res['middleName'])
                 +((res['lastName'] == undefined || res['lastName'] == null)?'':' '+res['lastName']) ;
                 //)
                 this.EmployeeNumber = res['number'];
                 this.Regular = res['regular'].toFixed(2);
                 this.Holiday = res['holiday'].toFixed(2);
                 this.Vacation = res['vacation'].toFixed(2);
                 this.Bonus = res['bonus'].toFixed(2);

                 this.RegularHour = res['regularHour'].toFixed(2);
                 this.HolidayHour = res['holidayHour'].toFixed(2);
                 this.VacationHour = res['vacationHour'].toFixed(2);
                 this.BonusHour = res['bonusHour'].toFixed(2);

                 this.RegularPayRate = res['regularPayRate'].toFixed(2);
                 this.HolidayPayRate = res['holidayPayRate'].toFixed(2);
                 this.VacationPayRate = res['vacationPayRate'].toFixed(2);
                 this.BonusPayRate = res['bonusPayRate'].toFixed(2);

                 this.RegularAssignedHours = res['regularAssignedHours'].toFixed(2);
                 this.HolidayAssignedHours = res['holidayAssignedHours'].toFixed(2);
                 this.VacationAssignedHours = res['vacationAssignedHours'].toFixed(2);
                 this.BonusAssignedHours = res['bonusAssignedHours'].toFixed(2);



                 this.HourlyPayRate = res['hourlyPayRate'].toFixed(2);
                 
                
                // this.IncomeTax = res['incomeTax'].toFixed(2); 
                // this.FederalTax = res['federalTax'].toFixed(2);
                //  this.Medical = res['medical'].toFixed(2);
                //  this.LifeInsurance = res['lifeInsurance'].toFixed(2);
                //  this.ProvidentFund = res['providentFund'].toFixed(2);

                 this.FederalTaxAmount = res['federalTaxAmount'].toFixed(2);
                 this.SocialSecurityTaxAmount	= res['socialSecurityTaxAmount'].toFixed(2);
                 this.MedicareTaxAmount	= res['medicareTaxAmount'].toFixed(2);
                 this.StateTaxAmount	= res['stateTaxAmount'].toFixed(2);
                 this.AdditionalFederalTaxAmount= res['additionalFederalTaxAmount'] === undefined ? 0 :res['additionalFederalTaxAmount'].toFixed(2); 
                 this.PFML_MedicalAmount	= res['pFML_MedicalAmount'] === undefined ? 0 :res['pFML_MedicalAmount'].toFixed(2);
                 this.PFML_LeaveAmount	= res['pFML_LeaveAmount'] === undefined ? 0 :res['pFML_LeaveAmount'].toFixed(2);
                 this.EmployeeDeductionAmount =res['employeeDeductionAmount'] === undefined ? 0 :res['employeeDeductionAmount'].toFixed(2);


                 this.BasicSalary = res['amount'].toFixed(2);
                 this.GrossSalary = res['gross'].toFixed(2);

                 this.PayorBankNumber = res['payorBankNumber'];
                 this.CheckNumber = res['checkNumber'];
                 this.CheckDate = res['checkDate'];
                 this.StartDate = res['startDate'];
                 this.EndDate = res['endDate'];
                 this.PaidDate = res['paidDate'];

                 this.OrganizationName = res['organizationName'];
                 this.OrganizationUrl = res['organizationUrl'];
                 this.OrganizationEmail = res['organizationEmail'];
                   
                 this.Allowances = res['allowances'].toFixed(2);
                 this.Dependents = res['dependents'].toFixed(2);
                 this.Exemptions = res['exemptions'].toFixed(2);
                 this.TotalEarning = res['totalEarning'].toFixed(2);
                 this.TotalDeduction = res['totalDeduction'].toFixed(2);
                 this.TotalPay = (this.TotalEarning - this.TotalDeduction).toFixed(2);
                 this.Check = true;
                }))
             } 
            })
          }
          else{
            this.router.navigate(['/']);
          }
  }

}


