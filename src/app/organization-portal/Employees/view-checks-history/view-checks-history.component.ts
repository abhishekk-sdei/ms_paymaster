import { Component, OnInit,ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { Router } from '@angular/router';
import { ViewChecksDetailComponent } from '../view-checks-detail/view-checks-detail.component';
import {MatDialog, MatDialogConfig} from "@angular/material";
import {animate, state, style, transition, trigger} from '@angular/animations';
import * as moment from 'moment';
import { MatTableDataSource, MatSort, PageEvent, MatSelect } from '@angular/material';
import { LoaderService } from 'src/app/Common/loader.service'
@Component({
  selector: 'app-view-checks-history',
  templateUrl: './view-checks-history.component.html',
  styleUrls: ['./view-checks-history.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]), ],
})
export class ViewChecksHistoryComponent implements OnInit {
  pageNumber: number = 1;
  FilterTimePunches : FormGroup;
  pageSize:number = 10;
  totalRecords: number;
  totalPages:number;
  Isshowtabledata:number=0;
  Checkslist=[];
  TaxDeductionList = [];
  employeedropdownlist=[];
  PeriodList = [];
  expanded: CheckesDetails;
  CheckId :number;
  employeelist=[];
  SearchText:any = '';
  PayPeriodId:any;
  EmployeeId: any;
  EmployeecheckData:CheckesDetails[];
  EmployeeData:EmployeeDetails[];
  
  displayedColumns: string[] = ['StartDate','EndDate','EmployeeName', 'CheckNumber','CheckDate',

  'Gross','TotalDeductionAmount', 'Amount','Action'];
  //CheckesDetailsData:CheckesDetails[];

  SubdisplayedColumns: string[] = [
  //   'PayPeriodName',
  // 'CheckNumber',
  'TypeCode',
  'InDateTime',
  'OutDateTime',
  'RefereceDay',
  'ReferenceDate',        
  'HoursClock',
  'Hours',
  'HourlyRate',
  'OvertimeMultiplier',
  'Amount',];
  EmployeeSource = new MatTableDataSource(this.EmployeeData);
  EmployeeCheckSource = new MatTableDataSource(this.EmployeecheckData);

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private LoaderService:LoaderService,private router: Router,private empservices:EmployeeService,private dialog: MatDialog,private formBuilder: FormBuilder) { }

  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1)
    {
      
      this.FilterTimePunches = this.formBuilder.group({
        payPeriodId: [''],
        employeeID: [''],

      });
    this.EmployeeCheckSource.sort=this.sort;
    this.GetEmployeeCheckslistHistory(0,0,this.SearchText,this.pageNumber, this.pageSize);
    this.getemployeedropdown();
    this.empservices.GetPayPeriodList().subscribe((res=>{
      if(res){
        debugger
        this.PeriodList=res||[];
        // console.log(res);
      }else{

      }
    }));
    }
    else{
      this.router.navigate(['/']);
    }
  }

  openDialogViewCheckDetail(EMPLOYEEID:number,PayperiodId : number)
  {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = false;
    dialogConfig.data={
      EmployeeId:EMPLOYEEID,
      PayperiodId : PayperiodId
    }

   
    const dialogRef =this.dialog.open(ViewChecksDetailComponent,dialogConfig);

    // dialogRef.afterClosed().subscribe(confirmresult=>{ 
    //   debugger 
       
    //    this.GetEmployeeCheckslist(this.pageNumber, this.pageSize);
    //   })  
      // return test;
   }

   click(){
    this.expanded =null;
    this.GetEmployeeCheckslistHistory(0,0,this.SearchText,this.pageNumber, this.pageSize);
   // this.LoaderService.hide();
  }
  
  GetEmployeeCheckslistHistory(PayPeriodId:number,EmployeeId:number,SearchText:any,PageNumber:number,PageSize:number) {
    this.empservices.GetEmployeeCheckslistHistory(PayPeriodId,EmployeeId,PageNumber, PageSize,SearchText).subscribe((res=>{
      if(res){
        debugger;
        let EmployeCheckearray = [];
        //if(res.data.length>0){
          this.Isshowtabledata=1
          
        this.Checkslist=res.data;
       
        this.Checkslist.forEach(function(value){
          debugger
          let Empcheckdata={
            CheckID : value['checkId'],
            EMPLOYEEID:value['employeeId'],
            PayperiodId:value['payperiodId'],
            EmployeeName:value['firstName']+
            (value['middleName'] == null? '':' '+value['middleName'])+
            (value['lastName'] == null? '':' '+value['lastName']),
            CheckNumber:value['checkNumber'],
            Gross:value.gross == null ? '0.00':value.gross.toFixed(2), //value['gross'], //.toFixed(2),
            Amount:value['amount'],
            CheckDate: moment(value['checkDate']).format('MM/DD/YYYY') ,
            StartDate: moment(value['startDate']).format('MM/DD/YYYY') ,
            EndDate: moment(value['endDate']).format('MM/DD/YYYY'),
            TotalDeductionAmount : value['totalDeductionAmount'] == null ? '0.00' : value['totalDeductionAmount'].toFixed(2)
            // FederalTaxAmount : value['federalTaxAmount'] === undefined || value['federalTaxAmount'] === null ? '0.00' :value['federalTaxAmount'].toFixed(2), 
            // SocialSecurityTaxAmount	:  value['socialSecurityTaxAmount']  === undefined || value['socialSecurityTaxAmount'] === null ? '0.00' :value['socialSecurityTaxAmount'].toFixed(2), 
            // MedicareTaxAmount : value['medicareTaxAmount'] === undefined || value['medicareTaxAmount'] === null ? '0.00' :value['medicareTaxAmount'].toFixed(2),  
            // StateTaxAmount	: value['stateTaxAmount'] === undefined || value['stateTaxAmount'] === null ? '0.00' :value['stateTaxAmount'].toFixed(2), 
            // AdditionalFederalTaxAmount : value['additionalFederalTaxAmount'] === undefined || value['additionalFederalTaxAmount'] === null ? '0.00' :value['additionalFederalTaxAmount'].toFixed(2),
            // PFML_MedicalAmount	: value['pFML_MedicalAmount'] === undefined || value['pFML_MedicalAmount'] === null ? '0.00' :value['pFML_MedicalAmount'].toFixed(2),
            // PFML_LeaveAmount	: value['pFML_LeaveAmount'] === undefined || value['pFML_LeaveAmount'] === null ? '0.00' :value['pFML_LeaveAmount'].toFixed(2),
            // EmployeeDeductionAmount : value['employeeDeductionAmount'] === undefined || value['employeeDeductionAmount'] === null || value['employeeDeductionAmount'] === 0 ? '0.00' :value['employeeDeductionAmount'].toFixed(2)

          }
          EmployeCheckearray.push(Empcheckdata);
        })
        this.EmployeecheckData=EmployeCheckearray;
        this.totalRecords=parseInt(res.meta["totalRecords"]);
        this.totalPages=parseInt(res.meta["totalPages"]);
        this.EmployeeCheckSource.data = this.EmployeecheckData
      // }
      // else{
      //   this.Isshowtabledata=2;
      //   this.totalRecords=0;
      //   this.totalPages=0;
      // }
      }

    }))
  }

  getemployeedropdown(){
    var Employeelist=[];
   
  
   this.empservices.BindPreviousEmployeeList().subscribe(res=>{
    if(res){
      // this.employeedropdownlist=res.data;
      this.employeedropdownlist=res||[];
     
    
      // this.DropDownemployeelist=this.employeelist.filter(x=>x.)
    }
  })
  }

  SearchingChecks(event:any){
    debugger
    this.SearchText = event.target.value;
    this.GetEmployeeCheckslistHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,1, this.pageSize,)
  }
  OnPayPeriodSelect(event:MatSelect){
    debugger
    this.PayPeriodId = event.value;
    this.GetEmployeeCheckslistHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,this.pageNumber, this.pageSize)
  }
  OnEmployeeSelect(event:MatSelect){
    debugger
    this.EmployeeId = event.value;
    this.GetEmployeeCheckslistHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,this.pageNumber, this.pageSize)
  }

  onClick(element: CheckesDetails) {
         

    // let index = this.expandedElements.findIndex(x => x == element.EMPLOYEEID);
    // if(index > -1)
    //   this.expandedElements.splice(index, 1);
    // else {
    //   this.expandedElements.push(element.EMPLOYEEID)
    // }
    debugger
      this.expanded = this.expanded === element ? null : element;
      this.CheckId=parseInt(element.CheckID);
      this.PayPeriodId = parseInt(element.PayperiodId)
      this.EmployeeSource.data =[];
      // this.GetEmployeeTimepunches(element.EMPLOYEEID);
      this.GetTimePunchHistory(this.PayPeriodId,parseInt(element.EMPLOYEEID),"",1,100);
      // this.GetEmployeeList();
    }
    GetTimePunchHistory(PayPeriodId:number,EmployeeId:number,SearchText:any,PageNumber:number,PageSize:number)
    {
      debugger
      this.empservices.GetTimePunchHistory(PayPeriodId,EmployeeId,PageNumber, PageSize,SearchText).subscribe(res=>{
        if(res){
          // this.employeedropdownlist=res.data;
          //this.checksdropdownlist=res||[];
          debugger;
          this.employeelist=res.data;
  
  
          let Employeearray = [];
          this.employeelist.forEach(function(value){
            debugger
            let employeedata={
            
            EMPLOYEEID:     value['employeeId'],
            TypeCode:       value['typeCode'],
            PayPeriodName:  value['payPeriodName'],
            Number:         value['number'],
            EmployeeName:   value['employeeName'],
            SocSec:         value['socSec'],
            CheckNumber:    value['checkNumber'],
            HourlyRate:     parseFloat(value['hourly_PayRate']).toFixed(2),
            Hours:          parseFloat(value['hours']).toFixed(2),
            HoursClock:     value['hoursClock'].toFixed(2),
            Amount:         parseFloat(value['amount']).toFixed(2),
            InDateTime:     value['inDateTime'],
            OvertimeMultiplier:     parseFloat(value['overtimeMultiplier']).toFixed(2),
            OutDateTime:    value['outDateTime'],
            RefereceDay:    value['refereceDay'],
            ReferenceDate:  value['referenceDate'],
            
          }
          Employeearray.push(employeedata);
          })
          this.EmployeeData=Employeearray;
  
          this.totalRecords=parseInt(res.meta["totalRecords"]);
          this.totalPages=parseInt(res.meta["totalPages"]);
          
          // console.log(res);
          this.EmployeeSource.data = this.EmployeeData
          // this.EmployeeTimePunchesSource.data = this.EmployeeData
        
          // this.DropDownemployeelist=this.employeelist.filter(x=>x.)
        }
      })
    }

  pageEvent(event: PageEvent) {
    this.pageNumber = event.pageIndex + 1;
    this.GetEmployeeCheckslistHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,this.pageNumber, this.pageSize);
  }

}

export interface CheckesDetails {
  EMPLOYEEID:string
  CheckID:string,
  PayperiodId:string,
  Gross:string,
  Amount:string,
  CheckNumber:string,
  CheckDate:string,
  EmployeeName:string
}
export interface TaxDeductionsDetails {
  Deductions : string,
  Amount : string
}

export interface EmployeeDetails {
  EMPLOYEEID:string
  FirstName:string,
  LastName:string,
  Number:string,
  HourlyRate:string,
  Hours:string,
  Amount:string,
  SocSec:string,
  Status:string,
  ACTION:string
}