import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChecksHistoryComponent } from './view-checks-history.component';

describe('ViewChecksHistoryComponent', () => {
  let component: ViewChecksHistoryComponent;
  let fixture: ComponentFixture<ViewChecksHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChecksHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChecksHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
