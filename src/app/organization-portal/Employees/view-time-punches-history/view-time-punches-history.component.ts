import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource, MatSort, PageEvent, MatSelect } from '@angular/material';
import * as moment from 'moment';
import { Router } from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-view-time-punches-history',
  templateUrl: './view-time-punches-history.component.html',
  styleUrls: ['./view-time-punches-history.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ViewTimePunchesHistoryComponent implements OnInit {
  
  FilterTimePunches : FormGroup;
  submitted: boolean;
  expanded: EmployeeDetails;
  status: boolean = false;
  shouldShow: boolean = false;
  pageNumber: number = 1;
  pageSize:number = 10;
  TimePunchCheck:number = 1;
  totalRecords: number;
  totalPages:number;
  EmployeeId: any;
  PayPeriodId:any;
  SearchText:any = '';
  employeelist=[];
  Typelist=[];
  employeedropdownlist=[];
  checksdropdownlist=[];
  employeeTimepuncheslist=[];
  EmployeeData:EmployeeDetails[];
  EmployeeTimePunchesData:EmployeeTimeDetails[];
  employeePayPeriodlist: any;
  currentpayperiodId: number;
  maxDate:any;
  minDate:any;
  EmployeeSource = new MatTableDataSource(this.EmployeeData);
  EmployeeTimePunchesSource = new MatTableDataSource(this.EmployeeTimePunchesData);
  displayedColumns: string[] = [
    'Number',
    'EmployeeName',
    'SocSec',
    'PayPeriodName',
    'CheckNumber',
    'TypeCode',
    'InDateTime',
    'OutDateTime',
    'RefereceDay',
    'ReferenceDate',        
    'HoursClock',
    'Hours',
    'HourlyRate',
    'OvertimeMultiplier',
    'Amount',
    
    ];

  TotalEmployeeAmount:any;
  TotalEmployeeHours:any;
  TotalEmployeeHoursClock: any;
  EmpId:any;
  PeriodList: any;
  noData: any;

  constructor(private EmployeeServices:EmployeeService,private router:Router,private formBuilder: FormBuilder) { }

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1){
    
      this.FilterTimePunches = this.formBuilder.group({
        payPeriodId: [''],
        employeeID: [''],

      });

    this.GetTimePunchHistory(0,0,this.SearchText,this.pageNumber, this.pageSize);
    debugger
    this.getemployeedropdown();
    

   // this.EmployeeTimePunchesSource.sort=this.sort;
    this.EmployeeServices.GetPayPeriodList().subscribe((res=>{
      if(res){
        debugger
        this.PeriodList=res||[];
        // console.log(res);
      }else{

      }
    }));
  }
  else{
    this.router.navigate(['/']);
  }
  }
  ngAfterViewInit() {
   
    this.EmployeeSource.sort = this.sort;
  }
  get FilteredTimepunches() { return this.FilterTimePunches.controls; }
  


  getemployeedropdown(){
    //var Employeelist=[];
   
    debugger
   this.EmployeeServices.BindPreviousEmployeeList().subscribe(res=>{
    if(res){
      debugger
      // this.employeedropdownlist=res.data;
      this.employeedropdownlist=res||[];
    }
  })
  }
  SearchingChecks(event:any){
    debugger
    this.SearchText = event.target.value;
    this.GetTimePunchHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,1, this.pageSize,)
  }
  OnPayPeriodSelect(event:MatSelect){
    debugger
    this.PayPeriodId = event.value;
    this.GetTimePunchHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,this.pageNumber, this.pageSize)
  }
  OnEmployeeSelect(event:MatSelect){
    debugger
    this.EmployeeId = event.value;
    this.GetTimePunchHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,this.pageNumber, this.pageSize)
  }

  GetTimePunchHistory(PayPeriodId:number,EmployeeId:number,SearchText:any,PageNumber:number,PageSize:number)
  {
    debugger
    this.EmployeeServices.GetTimePunchHistory(PayPeriodId,EmployeeId,PageNumber, PageSize,SearchText).subscribe(res=>{
      if(res){
        // this.employeedropdownlist=res.data;
        //this.checksdropdownlist=res||[];
        debugger;
        let Employeearray = [];
      
         if(res.data.length>0){

         
          this.employeelist=res.data;

          this.employeelist.forEach(function(value){
            debugger
            let employeedata={
            
            EMPLOYEEID:     value['employeeId'],
            TypeCode:       value['typeCode'],
            PayPeriodName:  value['payPeriodName'],
            Number:         value['number'],
            EmployeeName:   value['employeeName'],
            SocSec:         value['socSec'],
            CheckNumber:    value['checkNumber'],
            HourlyRate:     parseFloat(value['hourly_PayRate']).toFixed(2),
            Hours:          parseFloat(value['hours']).toFixed(2),
            HoursClock:     value['hoursClock'].toFixed(2),
            Amount:         parseFloat(value['amount']).toFixed(2),
            InDateTime:     value['inDateTime'],
            OvertimeMultiplier:     parseFloat(value['overtimeMultiplier']).toFixed(2),
            OutDateTime:    value['outDateTime'],
            RefereceDay:    value['refereceDay'],
            ReferenceDate:  value['referenceDate'],
            
          }
          Employeearray.push(employeedata);
          })
          this.EmployeeData=Employeearray;
  
          this.totalRecords=parseInt(res.meta["totalRecords"]);
          this.totalPages=parseInt(res.meta["totalPages"]);
          
        
          this.EmployeeSource.data = this.EmployeeData
        }
        else{
          this.EmployeeSource.data=Employeearray;
          this.noData = this.EmployeeSource.connect().pipe(map(data => data.length === 0));
          this.totalRecords=0;
          this.totalPages=0;
        }
      }
    })
  }
  GetEmployeeTimeList(){
    this.EmployeeServices.ViewEmployeeTimeList(this.pageNumber, this.pageSize).subscribe((res=>{
      if(res)
      {
        debugger;
        this.employeelist=res.data.timePunhesViewModel;

        this.employeePayPeriodlist=res.data.employeePayPeriodModel;
        this.currentpayperiodId=this.employeePayPeriodlist.payPeriodId;
        
        this.minDate=new Date(this.employeePayPeriodlist.startDate);
        this.maxDate=new Date(this.employeePayPeriodlist.endDate);

        let Employeearray = [];
        this.employeelist.forEach(function(value){
          let employeedata={
          
          EMPLOYEEID:value['employeeId'],
          Number:value['number'],
          FirstName:value['firstName'],
          LastName:value['lastName'],
          HourlyRate:parseFloat(value['hourly_PayRate']).toFixed(2),
          Hours:parseFloat(value['totalTime']).toFixed(2),

          HoursClock:value['hoursClock'],
          SocSec:value['socSec'],
          // Amount:value['Amount']
          Amount:parseFloat(value['basicSalary']).toFixed(2),
          Status : value["isActive"] == 1 ?'Active':'In-Active'
        }
        Employeearray.push(employeedata);
        })
        this.EmployeeData=Employeearray;

        this.totalRecords=parseInt(res.meta["totalRecords"]);
        this.totalPages=parseInt(res.meta["totalPages"]);
        
        // console.log(res);
        this.EmployeeSource.data = this.EmployeeData
        // this.EmployeeTimePunchesSource.data = this.EmployeeData
      }
      else{
        
    }return res;}))
  }
  onClick(element: EmployeeDetails) {
    debugger;     
    this.expanded = this.expanded === element ? null : element;
    this.EmpId=parseInt(element.EMPLOYEEID);
    this.EmployeeTimePunchesSource.data =[];
    // this.GetEmployeeTimepunches(element.EMPLOYEEID);
    this.GetEmployeeTimepunches(this.EmpId);
    // this.GetEmployeeList();
  }
  pageEvent(event: PageEvent) {
    debugger
    this.pageNumber = event.pageIndex + 1;
    this.GetTimePunchHistory(this.PayPeriodId,this.EmployeeId,this.SearchText,this.pageNumber, this.pageSize);
  }
  getHoursDiff(element: EmployeeTimeDetails) {
    
    let IN = moment(element.IN, "HH:mm:ss ");
    let Out = moment(element.Out, "HH:mm:ss a");
    let duration = moment.duration(Out.diff(IN));
    element.Hrsclock= duration.asHours().toFixed(2);
    return duration.asHours().toFixed(2);
  }
  GetEmployeeTimepunches(Emolyeeid:number){
    let tempEmpID = Emolyeeid;
    this.EmployeeServices.ViewEmployeeTimepunches(Emolyeeid).subscribe((res=>{
      if(res){
        this.employeeTimepuncheslist=res;
        let EmployeeTimearray = [];
        
        this.employeeTimepuncheslist.forEach(function(value){
          debugger
          let employeedata={
            checkId:value["checkId"],
            TimePunchId:value["timePunchId"],
            Type:value['type'].trim(),
            // IN:value['inDateTime'],
            // Out:value['outDateTime'],
            IN:value['inDateTime']? moment(value['inDateTime']).format("hh:mm"): "12:00",
            Out:value['outDateTime']?moment(value['outDateTime']).format("hh:mm a"): "12:00",
            Date:value['createdDate'],
            HourlyRate:value['hourlyPayRate'] == null ? 0:value['hourlyPayRate'],
            Hours:value['hours'] == null ? 0:value['hours'],
            HoursClock:value["hoursClock"] == null?0:value["hoursClock"],
            OVT:value['overtimeMultiplier'] == null?0:value['overtimeMultiplier'],
            Amount:value['amount'] == null ? "0.00" :value['amount'].toFixed(2)
            
          
        }
        EmployeeTimearray.push(employeedata);
        })
        this.EmployeeTimePunchesData=EmployeeTimearray;
        this.EmployeeTimePunchesSource.data = this.EmployeeTimePunchesData
        debugger
        this.TotalEmployeeAmount=  this.EmployeeTimePunchesData.map(x=>parseFloat(x.Amount)).reduce((acc,amount)=>acc+amount,0).toFixed(2);
        this.EmployeeSource.data.filter(x=>x.EMPLOYEEID==tempEmpID.toString())[0].Amount=this.TotalEmployeeAmount;
       
      }
    }))
  }
  
}

export interface EmployeeDetails {
  EMPLOYEEID:string
  FirstName:string,
  LastName:string,
  Number:string,
  HourlyRate:string,
  Hours:string,
  Amount:string,
  SocSec:string,
  Status:string,
  ACTION:string
}
export interface EmployeeTimeDetails {
  TimePunchId:string,
  checkId:string,
  Type:string
  IN:string,
  Out:string,
  Date:string,
  HourlyRate:string,
  Hours:string,
  Hrsclock:string,
  Amount:string,
  OVT:string,
}