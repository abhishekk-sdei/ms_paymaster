import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTimePunchesHistoryComponent } from './view-time-punches-history.component';

describe('ViewTimePunchesHistoryComponent', () => {
  let component: ViewTimePunchesHistoryComponent;
  let fixture: ComponentFixture<ViewTimePunchesHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTimePunchesHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTimePunchesHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
