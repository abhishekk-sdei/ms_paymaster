import { Component, OnInit,ViewChild } from '@angular/core';
import {PageEvent} from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-payperiod',
  templateUrl: './payperiod.component.html',
  styleUrls: ['./payperiod.component.scss']
})
export class PayperiodComponent implements OnInit {
  pageNumber: number = 1;
  pageSize:number = 10;
  totalRecords: number;
  totalPages:number;
  processPayrollElement : any;
  PayPeriodList=[];
  PayPeriodData:PayPeriodDetails[];
  displayedColumns: string[] = [ 'PeriodName','StartDate','EndDate','CheckDate', 'StatusName','Action'];
  EmployeePayPeriodSource = new MatTableDataSource(this.PayPeriodData);
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  noData: any;
  
  constructor(private toastr: ToastrService,private empservices:EmployeeService,private router:Router) { }

  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1){
      this.EmployeePayPeriodSource.sort=this.sort;
      this.GetEmployeePayPeriodlist();
    }
    else{
      this.router.navigate(['/']);
    }
  }

  GetEmployeePayPeriodlist() {
    this.empservices.GetEmployeepayperiodList(this.pageNumber, this.pageSize).subscribe((res=>{
      if(res){
        let EmployeCheckearray = [];
        debugger;
        
        if(res.data.length>0)
        {
          
        this.PayPeriodList=res.data;
        
        this.PayPeriodList.forEach(function(value){
          debugger
          let Empcheckdata={
           
            PayPeriodId:value['payPeriodId'],
            StartDate:value['startDate'],
            EndDate:value['endDate'],
            CheckDate:value['checkDate'],
            TransferDate:value['transferDate'],
            Status:value['status'],
            StatusName:value['statusName'],
            PeriodName:value['periodName'] ,
            Action:value['processStatus'] ,
            PayFrequency : value['payFrequency'],
            ProcessCHK : value['processCHK']
          }
          EmployeCheckearray.push(Empcheckdata);
        })
        this.PayPeriodData=EmployeCheckearray;
        this.totalRecords=parseInt(res.meta["totalRecords"]);
        this.totalPages=parseInt(res.meta["totalPages"]);
        this.EmployeePayPeriodSource.data = this.PayPeriodData
      }
      else{
        
        this.EmployeePayPeriodSource.data=EmployeCheckearray;
        this.noData = this.EmployeePayPeriodSource.connect().pipe(map(data => data.length === 0));
        this.totalRecords=0;
        this.totalPages=0;
      }
    }

    })
    )
  }

  GetPayperiodListByName(event:any){
    if(event.target.value=="")
    {
      this.GetEmployeePayPeriodlist();
    }
    else{
      this.empservices.GetEmployeePayPeriodlistByName(event.target.value,this.pageNumber, this.pageSize).subscribe((res=>{
        if(res)
        {
          let PayPeriodArray = [];
          debugger;
          if(res.data.length>0)
          {
       
          this.PayPeriodList=res.data;
         
          this.PayPeriodList.forEach(function(value){
            let PayPeriodData={
             //EmployeeName:value['payPeriodName'],
             StartDate:value['startDate'],
             EndDate:value['endDate'],
             CheckDate:value['checkDate'],
             TransferDate:value['transferDate'],
             Status:value['status'],
             StatusName:value['statusName'],
             PeriodName:value['periodName'] ,
             Action:value['processStatus'] ,
             ProcessCHK : value['processCHK']
          }
          PayPeriodArray.push(PayPeriodData);
          })
          this.PayPeriodData=PayPeriodArray;
          
          this.totalRecords=parseInt(res.meta["totalRecords"]);
          this.totalPages=parseInt(res.meta["totalPages"]);
          
          // console.log(res,this.EmployeePayPeriodSource);
          this.EmployeePayPeriodSource.data = this.PayPeriodData
        }
        else{
          this.EmployeePayPeriodSource.data=PayPeriodArray;
          this.noData = this.EmployeePayPeriodSource.connect().pipe(map(data => data.length === 0));
          this.totalRecords=0;
          this.totalPages=0;
        
        }
      }
        else{
          
        }return res;
      }))
    }
    
  }
 pageEvent(event: PageEvent) {
    this.pageNumber = event.pageIndex + 1;
    this.GetEmployeePayPeriodlist();
  }
  DialogProcessPayroll(e)
  {
    this.processPayrollElement = e;
  }

  OnPayrollProcess(){
    debugger
    // console.log(element)
    let PayPeriodId=parseInt(this.processPayrollElement.PayPeriodId);

    debugger
    const payrollProcessDetail={
      PayPeriodId:PayPeriodId,
      userId:parseInt(localStorage.getItem("UserID")),
      createDate : this.processPayrollElement.StartDate,
      payFrequency : this.processPayrollElement.PayFrequency,
      applicationId:0,
      createdBy : parseInt(localStorage.getItem("UserID")),
      checkDate : this.processPayrollElement.CheckDate
    }
    this.empservices.Paypayrollprocess(payrollProcessDetail).subscribe((res=>{
      // if (res) {
      // this.toastr.success(res.message);
      // this.GetEmployeePayPeriodlist();
      // }
      if (res) {
        debugger
        if(res.statusCode==200){
          debugger
           this.GetEmployeePayPeriodlist();
           this.toastr.success(res.message);
           this.empservices.AddCheckIdInTaxDeduction(payrollProcessDetail).subscribe((res=>{
            // if (res) {
            // this.toastr.success(res.message);
            // this.GetEmployeePayPeriodlist();
            // }
            if (res) {
              debugger
              if(res.statusCode==200){
                
              }
            
            }
          }))
        }else{
          this.toastr.error(res.message);
        }
      
      }
    }))
    
    
  }
 
}
export interface PayPeriodDetails {
  EMPLOYEEID:string
  CheckID:string,
  PayPeriodId:string,
  Gross:string,
  Amount:string,
  CheckNumber:string,
  CheckDate:string,
  EmployeeName:string
} 