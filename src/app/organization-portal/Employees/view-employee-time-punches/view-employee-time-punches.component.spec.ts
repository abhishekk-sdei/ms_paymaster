import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEmployeeTimePunchesComponent } from './view-employee-time-punches.component';

describe('ViewEmployeeTimePunchesComponent', () => {
  let component: ViewEmployeeTimePunchesComponent;
  let fixture: ComponentFixture<ViewEmployeeTimePunchesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEmployeeTimePunchesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEmployeeTimePunchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
