import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { EmployeeService } from 'src/app/Services/EmployeeServices';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import * as moment from 'moment';


@Component({
  selector: 'app-view-employee-time-punches',
  templateUrl: './view-employee-time-punches.component.html',
  styleUrls: ['./view-employee-time-punches.component.scss',] 
})
export class ViewEmployeeTimePunchesComponent implements OnInit {
  SubdisplayedColumns: string[] = ['Type','Date', 'Day', 'IN', 'Out','Hrsclock','Hours','HourRate','OVT','Amount'];
  employeeTimepuncheslist=[]; 
  EmployeeTimePunchesData:EmployeeTimeDetails[];
  EmployeeTimePunchesSource = new MatTableDataSource(this.EmployeeTimePunchesData);
  EmployeeName: string;
  EmployeeNo: any;
  TotalEmployeeAmount:any;
  TotalEmployeeHours:any;
  TotalEmployeeHoursClock: any;
  EmpId: number;
  Check : boolean = false;
  EmpHrs: any;
  constructor(private router:Router, @Inject(MAT_DIALOG_DATA) public data:any,private EmployeeServices:EmployeeService,private route: ActivatedRoute) { }
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  ngOnInit() {
    if(localStorage.getItem("accessToken").length>1){

    this.route.queryParams.subscribe(params => {
      // if (params['EmpId'] > 0) {
        if (this.data.EmployeeId> 0) {
        // this.EmpId=parseInt(params['EmpId']);
        this.EmployeeTimePunchesSource.sort = this.sort;
        this.EmpId=this.data.EmployeeId;
        this.getEmployeedetails(this.EmpId);
        this.GetEmployeeTimepunches(this.EmpId);
        
      }
    })
  }
  else{
    this.router.navigate(['/']);
  }
  }
  GetEmployeeTimepunches(Emolyeeid:number){

    this.EmployeeServices.GetEmployeeTimepunches(Emolyeeid).subscribe((res=>{
      if(res){
        debugger;
        this.employeeTimepuncheslist=res;
        let EmployeeTimearray = [];
        this.employeeTimepuncheslist.forEach(function(value){
          let employeedata={
          checkId:value["checkId"],
          TimePunchId:value["timePunchId"],
          Type:value['type'],
          // IN:value['inDateTime']? moment(value['inDateTime']).format("hh:mm"): "9:30",
          // Out:value['outDateTime']?moment(value['outDateTime']).format("hh:mm"): "18:30",
          IN:value['inDateTime']? moment(value['inDateTime']).format("hh:mm"): "12:00",
          Out:value['outDateTime']?moment(value['outDateTime']).format("hh:mm a"): "12:00",
          Date:value['createdDate'],
          HourlyRate:value['hourlyPayRate'],
          Hrsclock:value['hoursClock'],
          Hours:value['hours'],
          OVT:value['overtimeMultiplier'],
          Amount:value['amount'],
          // Action:''
         
        }
        EmployeeTimearray.push(employeedata);
        })
        this.EmployeeTimePunchesData=EmployeeTimearray;
        this.EmployeeTimePunchesSource.data = this.EmployeeTimePunchesData
      
      }
    }))

   
  }

  getEmployeedetails(Emolyeeid:number){
    this.EmployeeServices.GetEmployeeById(Emolyeeid).subscribe((res=>{
    
      this.EmployeeNo=res.data['number'];
      this.EmpHrs=res.data['hourlyPayRate'];
      this.EmployeeName=res.data['employeeName']//res.data['firstName'] +' '+res.data['lastName'];
      this.Check  = true;
    }))
  }
  getHoursDiff(element: EmployeeTimeDetails) {
    
    let IN = moment(element.IN, "HH:mm:ss ");
    let Out = moment(element.Out, "HH:mm:ss a");
    let duration = moment.duration(Out.diff(IN));
    element.Hrsclock= duration.asHours().toFixed(2);
    return duration.asHours().toFixed(2);
  }

  getAmount(element:EmployeeTimeDetails){
    
    // if(element.Hours.length<1){
    //   element.Hours='0';
    // }
    // if(element.HourlyRate.length<1){
    //   element.HourlyRate='0';
    // }
    // if(element.OVT.length<1){
    //   element.OVT='0';
    // }
    let Hrsclock=parseFloat(parseFloat(element.Hrsclock).toFixed(2));
    let Hours=parseFloat(parseFloat(element.Hours || '0').toFixed(2));
    let HourlyRate=parseFloat(parseFloat(element.HourlyRate || '0').toFixed(2));
    let OVT=parseFloat(parseFloat(element.OVT || '0').toFixed(2));
    let amount;
    // if(Hrsclock<Hours)
    // {
    //    amount=HourlyRate * Hrsclock * OVT;
    // }
    // else{
      amount=HourlyRate * Hours * OVT;
    // }
   
    element.Amount = amount;


    return amount.toFixed(2);
  }

  getTotalAmount(){
    let arr = [...this.EmployeeTimePunchesSource.data];
    this.TotalEmployeeAmount= arr.map(x=>parseFloat(x.Amount)).reduce((acc,amount)=>acc+amount,0).toFixed(2);
    return arr.map(x=>parseFloat(x.Amount)).reduce((acc,amount)=>acc+amount,0).toFixed(2);
  }
  getHoursSum(){
    let arr = [...this.EmployeeTimePunchesSource.data];
    this.TotalEmployeeHours=parseFloat(arr.map(x=>parseFloat(x.Hours || '0')).reduce((acc,Hours)=>acc+Hours,0).toFixed(2));
    // return arr.map(x=>parseFloat(x.Hours)).reduce((acc,Hours)=>acc+Hours,0);

    return  this.TotalEmployeeHours.toFixed(2);
  
  }
  getHoursClock(){
    let arr = [...this.EmployeeTimePunchesSource.data];
    this.TotalEmployeeHoursClock= parseFloat(arr.map(x=>parseFloat(x.Hrsclock)).reduce((acc,Hrsclock)=>acc+Hrsclock,0).toFixed(2));
    // return   (arr.map(x=>parseFloat(x.Hrsclock)).reduce((acc,Hrsclock)=>acc+Hrsclock,0));
    return  this.TotalEmployeeHoursClock.toFixed(2);
  
  }
   


  OnblurMethod(element:EmployeeTimeDetails){
    // this.getHoursDiff(element);
    this.getHoursSum();
    this.getTotalAmount();
    this.getHoursClock();
    // element.Amount = element.Amount;
    element.Hours = element.Hours || '0';
    element.HourlyRate = element.HourlyRate || '0'
    element.OVT = element.OVT || '0';
    this.UpdateTimePunches(element);
  }

  async  UpdateTimePunches(element:EmployeeTimeDetails) {
    debugger;
    let arr = [...this.EmployeeTimePunchesSource.data];
    let Timepunches=[];
    let id=this.EmpId;
    let EmpHrs =this.EmpHrs
    let TotalEmployeeAmount=parseFloat(this.TotalEmployeeAmount);
    let TotalEmployeeHoursClock=parseFloat(this.TotalEmployeeHoursClock);
    let TotalEmployeeHours=parseFloat(this.TotalEmployeeHours);
    if(element.Out.toLowerCase().includes("am")||element.Out.toLowerCase().includes("pm")){
      element.Out=moment(element.Out, "h:mm:ss A").format("HH:mm")
    }
    else{
      element.Out=element.Out;
    }
    const timepunches={
          TimePunchId: element.TimePunchId,
          EmployeeId:id,
          // InDateTime:moment(element.IN).format("hh:mm"),
          InDateTime:element.IN,
         
          outDateTime:element.Out,
          // OutDateTime:moment(element.Out,"hh:mm:ss a").format("hh:mm"),
          checkId:element.checkId,
          ReferenceDate:moment(element.Date).format("YYYY-MM-DD"),
          Type:element.Type,
          Hours:parseFloat(element.Hours),
          HoursClock:element.Hrsclock,
          HourlyPayRate:parseFloat(element.HourlyRate),
          OvertimeMultiplier:parseFloat(element.OVT),
          Amount:parseFloat(element.Amount),
          TotalAmount:TotalEmployeeAmount,
          TotalHrsclock:TotalEmployeeHoursClock,
          TotalHrs:TotalEmployeeHours,
          EmpHourlyPayRate:EmpHrs
        }
    // arr.forEach(function(value){
    //   const timepunches={
    //     TimePunchId: value["TimePunchId"],
    //     EmployeeId:id,
    //     InDateTime:value["IN"],
    //     OutDateTime:value["Out"],
    //     Type:value["Type"],
    //     Hours:value["Hours"],
    //     HoursClock:value["Hrsclock"],
    //     HourlyPayRate:value["HourlyRate"],
    //     OvertimeMultiplier:value["OVT"],
    //     Amount:value["Amount"],
    //     TotalAmount:TotalEmployeeAmount,
    //     TotalHrsclock:TotalEmployeeHoursClock,
    //     TotalHrs:TotalEmployeeHours
    //   }
    //   Timepunches.push(timepunches);
    // })
   
    // console.log(arr,this.TotalEmployeeHoursClock,this.TotalEmployeeHours,this.TotalEmployeeAmount);
    
    var asyncResult= await this.EmployeeServices.updateTimepunches(timepunches).subscribe((res=>{
      console.log(res);
    }))
    // console.log(arr);
  }
  
  }
export interface EmployeeTimeDetails {
  TimePunchId:string,
  checkId:string,
  Type:string
  IN:string,
  Out:string,
  Date:string,
  HourlyRate:string,
  Hours:string,
  Hrsclock:string,
  Amount:string,
  OVT:string,
  // Action:string,
}