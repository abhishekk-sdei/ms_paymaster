import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonService } from './Services/Commonservices';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OrganizationPortalModule } from './organization-portal/organization-portal.module';
import { SubdomainService } from './subdomain.service';
import { AuthenticationService } from './Services/AuthenticationService';
import { JwtInterceptor } from './Common/jwt-interceptor';
import { EmployeeService } from './Services/EmployeeServices';
import { LoaderInterceptorService } from './Common/loader-interceptor.service';
//import { OnlyNumericDirective } from './Directive/only-numeric.directive';
@NgModule({
  declarations: [
    AppComponent
    //,OnlyNumericDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [EmployeeService,CommonService, SubdomainService,AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptorService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
