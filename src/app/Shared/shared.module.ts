import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { headerComponent } from './header/header.component';
import { sidebarComponent } from './sidebar/sidebar.component';
import { RouterModule, Routes, RouterState } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatSelectModule,
  MatIconModule, MatCheckboxModule, MatNativeDateModule, } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
  declarations: [
     headerComponent,
     sidebarComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDatepickerModule,
    RouterModule
  ],
  exports: [
    headerComponent,
     sidebarComponent,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatCheckboxModule,
    MatNativeDateModule,
    MatDatepickerModule,
    RouterModule
  ]
})
export class SharedModule { }
