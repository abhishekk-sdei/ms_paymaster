import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { AuthenticationService } from 'src/app/Services/AuthenticationService';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class headerComponent implements OnInit {
  constructor(private authentication: AuthenticationService,private router:Router) { }

  ngOnInit() {
  }

  
  slideNavbar() {
    $("body").toggleClass("slidenav");

  }

  OrganizationLogout(){
    localStorage.removeItem("accessToken");
    this.router.navigate(['/']);
    // =localStorage.getItem("accessToken");
  }
}
