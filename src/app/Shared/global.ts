import {environment} from '../../environments/environment'


export class GLOBAL{

    //#region CommonAuthAPI
    public static fetchdomainlist = environment.baseUrl + 'Home/VerifyDomainName';
    public static LoginUrl = environment.baseUrl + 'Organization/Login';
    //#endregion


    //#region Employees API
    public static GetEmployeelist = environment.baseUrl + 'Organization/GetEmployeeList';
    public static AddEmployee = environment.baseUrl + 'Employee/SaveEmployee';

    public static AddEmployeeBasicDetails = environment.baseUrl + 'Employee/SaveEmployeeBasicDetails';
    public static AddEmployeeSalaryDetails = environment.baseUrl + 'Employee/SaveEmployeeSalaryDetails';
    public static AddEmployeePersonalDetails = environment.baseUrl + 'Employee/SaveEmployeePersonalDetails';

    public static UpdateEmployeeBasicDetails = environment.baseUrl + 'Employee/UpdateEmployeeBasicDetails';
    public static UpdateEmployeeSalaryDetails = environment.baseUrl + 'Employee/UpdateEmployeeSalaryDetails';
    public static UpdateEmployeePersonalDetails = environment.baseUrl + 'Employee/UpdateEmployeePersonalDetails';

    public static UpdateEmployee = environment.baseUrl + 'Employee/UpdateEmployee';
    public static GetEmployeeId = environment.baseUrl + 'Organization/GetEmployeeById';
    public static DeletedEmployee = environment.baseUrl + 'Organization/DeleteEmployee';
    public static GetEmployeeListByName = environment.baseUrl + 'Organization/GetEmployeeListByName';
    public static CheckSocSecNo = environment.baseUrl + 'Organization/CheckSocSecNo';

    public static AddOrganization = environment.baseUrl + 'Login/SetUpOrganization';

    public static GetEmployeeTimelist = environment.baseUrl + 'TimePunch/GetEmployeeTimelist';
  
    public static GetTimePunchList = environment.baseUrl + 'TimePunch/GetTimePunchList';
    public static UpdateTimePunches = environment.baseUrl + 'TimePunch/UpdateTimePunch';
    public static GetEmployeecheckList = environment.baseUrl + 'TimePunch/GetEmployeesCheckslist';
    public static GetEmployeecheckListHistory = environment.baseUrl + 'TimePunch/GetEmployeecheckListHistory';

    public static GetChecksList = environment.baseUrl + 'TimePunch/GetDropdownBindCheckslist';
    public static GetTimePunchHistoryList = environment.baseUrl + 'TimePunch/GetTimePunchHistoryList';

    public static ViewChecksDetail = environment.baseUrl + 'TimePunch/ViewChecksDetails';
    public static AddTimePunches = environment.baseUrl + 'TimePunch/AddTimePunch';
    public static GetEmployeeCheckDeductionList = environment.baseUrl + 'TimePunch/GetEmployeeCheckDeductionList';
 
    public static GetPayPeriodList = environment.baseUrl + 'ProcessPayroll/GetPayPeriodList';
    public static GetPayPeriodListByName = environment.baseUrl + 'ProcessPayroll/GetPayPeriodListByName';
    public static PayProllProcess = environment.baseUrl + 'ProcessPayroll/PayProllProcess';
    public static AddCheckIdInTaxDeduction = environment.baseUrl + 'ProcessPayroll/AddCheckIdInTaxDeduction';
    
    public static Genderlist = environment.baseUrl + 'Common/GetGenderList';
    public static Racelist = environment.baseUrl + 'Common/GetRaceList';
    public static maritallist = environment.baseUrl + 'Common/GetMaritalStatusList';
    public static Countrylist = environment.baseUrl + 'Common/GetCountryList';
    public static statelist = environment.baseUrl + 'Common/GetStateList';
    public static citylist = environment.baseUrl + 'Common/GetCityList';
    public static typelist = environment.baseUrl + 'Common/GetTypeList';
    public static GetPayFrequencyList = environment.baseUrl + 'Common/GetPayPeriodList';
    public static ViewEmployeeTimelist = environment.baseUrl + 'TimePunch/ViewEmployeeTimelist';
    public static ViewTimePunchList = environment.baseUrl + 'TimePunch/ViewTimePunchList';
    public static BindPayPeriodList = environment.baseUrl + 'ProcessPayroll/GetAllPayPeriodList';
    public static BindActiveEmployee = environment.baseUrl + 'Organization/GetActiveEmployee';
    public static BindPreviousEmployee = environment.baseUrl + 'Organization/GetPreviousEmployee';
    public static FilteredTimepunchesList = environment.baseUrl + 'TimePunch/ViewEmployeeFilteredTimelist';

    public static GetIsChangesOnTimePuches = environment.baseUrl + 'ProcessPayroll/GetIsChangesOnTimePuches';
   public static GenerateEmployeeCheck=environment.baseUrl+ 'ProcessPayroll/PrintEmployeeChecks';

  public static Closepayrollprocess = environment.baseUrl + 'ProcessPayroll/closepayrollprocess';

  public static FromEmployeeList = environment.baseUrl + 'ProcessPayroll/FromEmployeeList';
  public static ToEmployeeList = environment.baseUrl + 'ProcessPayroll/ToEmployeeList';
    
   
    //#endregion

}