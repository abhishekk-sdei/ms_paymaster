import { Component, OnInit, OnDestroy } from '@angular/core';
import {SubdomainService} from './subdomain.service'
import { Subscription } from 'rxjs';
import { LoaderService } from './Common/loader.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit,OnDestroy{
  title = 'Mspaymaster';
  Isloading: boolean=false;
  show:boolean= false;
  private subscription: Subscription;
  constructor(private SubdomainService:SubdomainService,private loaderService: LoaderService){

  }
  ngOnInit() {
   let subdomainname= this.SubdomainService.GetSubDomainUrl();
   this.Isloading=true;
   this.SubdomainService.verifyDomainName(subdomainname).subscribe(res=>{
    this.Isloading = false;
    
   }, () => {
     this.Isloading = false;
   });


   this.subscription = this.loaderService.loaderState
   .subscribe((state: LoaderState) => {
     this.show = state.show;
     console.log(this.show);
   });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
export interface LoaderState {
  show: boolean;
}