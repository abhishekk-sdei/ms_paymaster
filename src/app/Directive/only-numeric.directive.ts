import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[numeric]'
})
export class OnlyNumericDirective {
  
  @Input('numericType') numericType: string; // number | decimal

  private regex = {
      //number: new RegExp(/^\d+$/),
      // decimal: new RegExp(/^[0-9]+(\.[0-9]*){0,1}$/g)
   //  decimal: new RegExp(/^[0-9]+(.[0-9]{0,2})?$/g)
     decimal:new RegExp(/^[0-9]+(?:\.[0-9]{0,2})?$/g) 
   // decimal:new RegExp(/^(?<!-)\+?\d+(\.?\d{0,2})?$/)
  };

  private specialKeys = {
      //number: [ 'Backspace', 'Tab', 'End', 'Home', 'ArrowLeft', 'ArrowRight' ],
      decimal: [ 'Backspace', 'Tab', 'End', 'Home', '-', 'ArrowLeft', 'ArrowRight' ],
  };
  constructor(private el: ElementRef) { }
  @HostListener('keydown', [ '$event' ])
  onKeyup(event: KeyboardEvent) {
    debugger;
     // if (this.specialKeys[this.numericType].indexOf(event.key) !== -1) {
      if (this.specialKeys[this.numericType].indexOf(event.key) !== -1 ) {
        
          return;
      } 
      //  if( this.el.nativeElement.value.split(".")[0].length <10) {
      //    return;
      //   }
      //   else 
      // if( this.el.nativeElement.value.split(".")[1].length <2) {
      //     return;
      //    }
      let current: string = this.el.nativeElement.value;
      const position = this.el.nativeElement.selectionStart;
      //let next: string = current.concat(event.key);
      const next: string = [current.slice(0, position), event.key, current.slice(position)].join('');
     
        if (next && !String(next).match(this.regex[this.numericType])) {
            event.preventDefault();
        }
     
      
  }
}
